import datetime
import json
import logging
import os
import sys
import time
from threading import Thread

from requests.packages.urllib3.exceptions import ReadTimeoutError
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener


class CollectTwitter(Thread):
    def __init__(self, folder, keys, keywords):
        super(CollectTwitter, self).__init__()
        self._folder = folder
        self._keys = keys
        self._keywords = keywords
        if not os.path.exists(self._folder):
            os.makedirs(self._folder)
        if not isinstance(keys, dict):
            raise ValueError("Keys must be dict")
        if 'consumer_key' not in keys or 'consumer_secret' not in keys:
            raise ValueError('Keys must contain consumer_key and consumer_secret')
        if 'access_token' not in keys or 'access_token_secret' not in keys:
            raise ValueError('Keys must contain access_token and access_token_secret')
        if not isinstance(keywords, list):
            raise ValueError("Keywords must be a list of keywords")
        self._is_timeout = False
        self._is_api_limit = False

    def run(self):
        try:
            auth = OAuthHandler(self._keys['consumer_key'], self._keys['consumer_secret'])
            auth.set_access_token(self._keys['access_token'], self._keys['access_token_secret'])
            listener = CollectTwitterListener(self._folder, self._keywords)
            twitterStream = Stream(auth, listener, retry_count=1, retry_time=1, timeout=10)
            twitterStream.filter(track='"{}"'.format(','.join(self._keywords)), languages=['fr',
                                                                                           'en'],
                                 async=False)  # Filter track
            self._is_api_limit = listener.is_api_limit
        except ReadTimeoutError:
            logging.error('Received a timeout error')
            self._is_timeout = True
        except Exception as e:
            logging.exception(e)

    @property
    def is_timeout(self):
        return self._is_timeout

    @property
    def is_api_limit(self):
        return self._is_api_limit


class CollectTwitterListener(StreamListener):

    INVALID_SITE = ['twitter.', 'youtube.', 'instagram.', 'swarmapp.', 'youtu.be', 'path.', 'vine.']

    SEPARATOR = '|||'

    def __init__(self, folder, keywords):
        super(CollectTwitterListener, self).__init__()
        self._folder = folder
        self._keywords = keywords
        self.count = 0
        self._is_api_limit = False

    def on_error(self, status_code):
        if status_code == 420:
            logging.warning(
                'Reach Api Rate limit, closing stream. More info @ https://dev.twitter.com/rest/public/rate-limiting')
            self._is_api_limit = True
            return False
        else:
            logging.warning(
                'Received %s status code from twitter api. More info @ https://dev.twitter.com/overview/api/response-codes',
                status_code)
            return True  # Don't kill the stream

    @property
    def is_api_limit(self):
        return self._is_api_limit

    def on_timeout(self):
        time.sleep(100)
        return True

    def on_disconnect(self, notice):
        logging.info('Disconnected from twitter stream')

    def on_data(self, raw_data):
        try:
            self.count += 1
            if self.count % 100 == 0:
                logging.info("#Items : %s", self.count)

            # === Json Conversion ===
            all_data = json.loads(raw_data)

            if 'id' not in all_data:
                # here we assume its not a tweet, example :
                # {'limit': {'track': 38, 'timestamp_ms': '1482940073291'}}
                return
            # === myTweet.loadTweet(data) ===
            tweetID = all_data['id']
            userID = all_data['user']['id']
            userFollowers = all_data['user']['followers_count']
            userLang = all_data['user']['lang']
            TweetText = all_data['text']
            favorited = all_data['favorite_count']
            retweet_count = all_data['retweet_count']
            # userLocation = all_data['user']['location']
            # TweetDate = all_data['created_at']

            if 'entities' not in all_data and 'urls' not in all_data['entities']:
                logging.warning('Missing url from this tweet : %s', TweetText)
                return
            urls = all_data['entities']['urls']

            # === Second Filter about my Personal BOW

            try:
                e_url = self.valid_url(urls)
                if self.has_keywords(TweetText) and e_url is not None and 'RT @' not in TweetText:
                    fileName = self.get_filename()
                    try:
                        # extraction
                        info = [str(tweetID), userLang, TweetText.replace('\n', ''), e_url
                            , str(favorited), str(retweet_count), str(userID),
                                str(userFollowers)]
                        result = self.SEPARATOR.join(info)
                        self.save(fileName, result)
                    except Exception as e:
                        print("Error Saving : ", e)
                else:
                    # logging.info('No urls for this tweet %s', TweetText)
                    pass
            except IndexError as ie:
                logging.info('Index error for urls %s, data was : %s, %s', urls, all_data, ie)
            except Exception as e:
                logging.exception(e)
                return True
        except Exception as e:
            logging.exception(e)
            return True

    def valid_url(self, urls):
        if len(urls) > 0 and 'expanded_url' in urls[0]:
            e_url = urls[0]['expanded_url']
            if not self.none_or_empty(e_url):
                if len([s for s in self.INVALID_SITE if s in e_url]) == 0:
                    return e_url
        return None

    def none_or_empty(self, str):
        return str == None or str == ''

    def has_keywords(self, tweet):
        for keyword in self._keywords:
            if keyword in tweet:
                return True
        return False

    def save(self, fileName, result):
        with open(os.path.join(self._folder, fileName), 'a', encoding='utf-8') as out:
            out.write(result)
            out.write('\n')

    def get_filename(self):
        d = datetime.datetime.now()
        day = d.day
        if d.hour >= 20:
            day += 1
        return "FeedTwitter-{}_{:02d}_{:02d}.txt".format(d.year, d.month, day)


def countdown(t):  # in seconds
    for i in range(t, 0, -1):
        sys.stdout.write('{} seconds remaining\r'.format(i))
        sys.stdout.flush()
        time.sleep(1)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    logging.info('Starting collect')
    from keys import keys

    max_sec = 120
    sec = 30
    while True:
        ct = CollectTwitter('output', keys, ['ai', 'deep learning'])
        ct.start()
        ct.join()
        if ct.is_api_limit:
            logging.info('Waiting 15 minutes.')
            countdown(15 * 60)
        elif not ct.is_timeout:
            logging.info('Stream was closed, restarting it in a few seconds : %s', sec)
            countdown(sec)
            sec = sec + 30 if sec < max_sec else max_sec
