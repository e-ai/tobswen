__author__ = 'Redo'

import json
import time
import concurrent.futures

from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
from keys import keys
from datetime import datetime
from http.client import IncompleteRead

# === consumer key, consumer secret, access token, access secret.
ckey=keys['consumer_key']
csecret=keys['consumer_secret']
atoken=keys['access_token']
asecret=keys['access_token_secret']
count = 0

# === Loading Domains BOW + Personal BOW ===
with open("_DomainsBOW.txt") as word_file:
    SetFilter = set(word.strip().lower() for word in word_file)
    MyFilter = str(SetFilter).replace('{', '"').replace('}', '"').replace("'", '')
    print(len(MyFilter), " - ", MyFilter)

with open("_PersonalBOW.txt") as word_file:
    MyWords = set(word.strip().lower() for word in word_file)
    print(MyWords)

# === Loading Languages ===
MyLangs = ['fr', 'en', 'en-gb']

# === List bane sites ===
site_no = ['twitter.', 'youtube.', 'instagram.', 'swarmapp.', 'youtu.be', 'path.', 'vine.']
separator = '|||'

# === Class & Def ===

class listener(StreamListener):

    count = 0

    def on_data(self, data):
        try:
            self.count += 1
            if self.count % 100 == 0: print("#Items : ", self.count)

            # === Json Conversion ===
            all_data = json.loads(data)

            # === myTweet.loadTweet(data) ===
            # if 'id' in all_data:
            tweetID = all_data['id']
            userID = all_data['user']['id']
            userFollowers = all_data['user']['followers_count']
            userLang = all_data['user']['lang']
            TweetText = all_data['text']
            try:
                urls = all_data['entities']['urls']
            except:
                urls = []
            # userLocation = all_data['user']['location']
            # TweetDate = all_data['created_at']

            favorited = all_data['favorite_count']
            retweet_count = all_data['retweet_count']

            # === Second Filter about my Personal BOW

            try:
                tempWords = [w for w in MyWords if w in TweetText]
                e_url = urls[0]['expanded_url']

                if len(tempWords) > 0 and len(e_url) > 0:

                    filter_url = [s for s in site_no if s in e_url]
                    if not ('RT @' in TweetText) and len(tempWords) > 0 and len(filter_url) == 0:

                        # Print for visualisation
                        dt = datetime.now().strftime("%A, %d. %B %Y %I:%M%p")
                        print(dt,
                              "#", len(tempWords),
                              "#", TweetText.replace("\n", " "),
                              " - ", urls[0]['expanded_url']
                              )

                        # Save in file
                        try :

                            # change the name of the file to switch at 8pm
                            d = datetime.now()
                            if d.hour >= 20:
                                fileName = "FeedTwitter-" + str(d.year) + "_" + str('{:02d}'.format(d.month))+ "_" + str('{'':02d}'.format(d.day+1)) + ".txt"
                            else:
                                fileName = "FeedTwitter-" + datetime.now().strftime('%Y_%m_%d') + ".txt"

                            # extraction
                            result = str(tweetID) + separator + \
                                     userLang + separator + \
                                     TweetText.replace("\n"," ") + separator + \
                                     e_url + separator + \
                                     str(favorited)  + separator + \
                                     str(retweet_count) + separator + \
                                     str(userID) + separator + \
                                     str(userFollowers)

                            SaveInFile(fileName, result)
                        except Exception as e:
                            print("Error Saving : ", e)

            except Exception as e:
                # print("Error Print: ", e, " - ", TweetText)
                return True

        except IncompleteRead:
            print("Error IncompleteRead")
            return True

        except Exception as e:
            print("Error Streamlistener : ", e)
            return True

    def on_error(self, status_code):
        if status_code == 420:
            return False
        else:
            print("status code", status_code)
            return True # Don't kill the stream

    def on_timeout(self):
        print("TimeOut")
        time.sleep(20)
        return True

    def on_disconnect(self, notice):
        print("disconnected")

def SaveInFile(File, Content):
    output1 = open(File,"a")
    output1.write(Content)
    output1.write('\n')
    output1.close()

def start_streaming(bFilter):
    try:
        auth = OAuthHandler(ckey, csecret)
        auth.set_access_token(atoken, asecret)
        twitterStream = Stream(auth, listener(), retry_count=1, retry_time=1, timeout=10)
        if bFilter == True:
            twitterStream.filter(track=[MyFilter], languages=MyLangs, async=False) # Filter track
        else:
            twitterStream.filter(locations=[-180,-90,180,90], async=True)
    except Exception as e:
        print(e)
    return twitterStream

def never_ending_stream(wait = 1):
    with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
        try:
            future = executor.submit(start_streaming, True)
            while future.running():
                time.sleep(wait)
            print(future.result())
            print('thread aborted, waiting 10sec before leaving')
            time.sleep(10)
        except KeyboardInterrupt:
            print('shutting down executor')
            executor._shutdown(wait=False)
            print('executor shutted down')
            return False
    return True

# === HERE ===
while True:
    if not never_ending_stream():
        print('breaking while')
        break


# def check_ping(hostname):
#     response = os.system("ping -c 1 " + hostname)
#     # and then check the response...
#     return response == 0
#
# def MyCode(bFilter=True):
#     auth = OAuthHandler(ckey, csecret)
#     auth.set_access_token(atoken, asecret)
#
#     try:
#         twitterStream = Stream(auth, listener(), retry_count=1, retry_time=1, timeout=10)
#         if bFilter == True:
#             twitterStream.filter(track=[MyFilter], languages=MyLangs, async=True) # Filter track
#         else:
#             twitterStream.filter(locations=[-180,-90,180,90], async=True) # No Filter + WW
#     except (IncompleteRead, HTTPException, ReadTimeoutError) as ex:
#         print('an error of type {0} occured, try to restart process. details : {1}', str(type(ex)), str(ex))
#         print('ping start')
#         while not check_ping('www.google.com'):
#             print('connection lost, waiting 5sec')
#             time.sleep(5)
#         if twitterStream is not None:
#             twitterStream.disconnect()
#         time.sleep(5)
#         MyCode(bFilter)
#         # print("erreur http")
#     except KeyboardInterrupt:
#         twitterStream.disconnect()
#     except Exception as e:
#         print('unhandled error')
#         print(e)