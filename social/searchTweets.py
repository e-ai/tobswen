__author__ = 'Redo'

import re
import nltk
import string
from gensim.models import Word2Vec
from os.path import expanduser
from collections import deque

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

home = expanduser("~") + '/DL_Models/'
model = Word2Vec.load(home + 'greg_model_test123')
# model.save('greg_model_test123')

lang = ['en']
fileName = "FeedTwitter-2016_08_17.txt"
nbrSents = 0 # use 0 for all items in list
simValue = 0.8 # similarity
w_V = 0.5 # weightVectors
w_S = 0.5 # weightSemantic
details = True

def stopwordsCustom():
    # Get default English stopwords and extend with punctuation
    stopwords = nltk.corpus.stopwords.words('english')
    stopwords.extend(string.punctuation)
    stopwords.append('')
    return stopwords

def loadFeeds():
    with open(fileName) as word_file:
        return set((sent.strip().lower() for sent in word_file))

def TraitFeedsList(myList):
    # List all items in the substract
    newList = []
    stopword = stopwordsCustom()
    for sent in myList:
        extract = sent.split("|")
        # print(str(len(extract)) + " - ", extract)
        if len(extract) > 3 and extract[1] in lang and len(extract[2]) > 10:
            # texte cleaning
            temp = re.sub("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", '',
                      extract[2])
            temp = re.sub(r'[?|$|.|!|#|/|(|)|&|*|"|@|:|;|{|}|''|.]',r'',temp)
            temp = temp.replace("'", "")
            # extract[2] = temp.strip().split()
            extract[2] = [word for word in temp.split(" ") if word not in stopword]
            newExtract = []
            for word in extract[2]:
                try:
                    vector = model[word]
                    newExtract.append(word)
                except:
                    continue
            # append
            extract[2] = newExtract
            newList.append(extract)

    if nbrSents == 0:
        return newList
    else:
        return  newList[0:nbrSents]

def computeSimilarity(S1, S2, v=0.5, s=0.5):

    similarity = 0

    # compute the vectorial similarity
    similarity_score = model.n_similarity(S1, S2)
    # cSim = round(similarity_score, 2)*100

    # compute the ratio of th correspondance words between the 2 tokenised sentences
    Ratio = (100 / (len(S1) + len(S2)) - len(set(S1).intersection(S2)))* len(set(S1).intersection(S2))
    # cRatio = round(cRatio, 2)

    # compute the addition of the ratio of the vectors similarity % and words % correspondance
    similarity = (similarity_score*v) + (Ratio*s)

    return similarity

FeedsList = loadFeeds()
list1 = TraitFeedsList(FeedsList)
otherList = []
resultList = []

count = 0
lenTrue = 0
count_errors = 0

# print("# - Similar - twitterID S1 - twitterID S2 -  % Vector Similarity - % words Correspondance - tokens S1 + S2")
print("Lines loaded : ", len(FeedsList))
print()

# === New Method based on Queues ===
# https://docs.python.org/3.4/tutorial/datastructures.html | 5.1.2.
# the idea is to present a table with sub element directly attached

print("Dynamic Clusterisation")

queue = deque(list1)

finalList = []
temp_queue = []

current_Item = queue.popleft()
current_Item.append([current_Item[0]])
temp_queue.append(current_Item)
pos = len(current_Item)-1

while len(queue) > 0:
    next_Item = queue.popleft()
    try:
        maxScore = 0
        index = -1
        index_temp_queue = -1
        for item in temp_queue:
            index += 1
            # score = model.n_similarity(item[2], next_Item[2])
            score = computeSimilarity(item[2], next_Item[2], w_V, w_S)
            if score > maxScore:
                maxScore = score
                index_temp_queue = index
                temp_ID = next_Item[0]
        if maxScore > simValue:
            temp_queue[index_temp_queue][pos].append(next_Item[0])
        else:
            next_Item.append([next_Item[0]])
            temp_queue.append(next_Item)
    except Exception as e:
        print(next_Item, " - ", e)
        count_errors += 1


print()
print()

for item in temp_queue:
    nbr = len(item[pos])
    print(nbr, " - ", item)

print()
print("{} dynamic Clusters based on similarity of {}, weight of vectors {} and weight of semantic {}".format(len(
    temp_queue),simValue, w_V, w_S))
print("Errors number : {}".format(count_errors))
print()
print("=======")

if details:

    print()

    for Item in temp_queue:
        if len(Item[pos]) > 1:
            print(Item)
            f_list = [item for item in list1 if item[0] in Item[pos]]
            if len(f_list) > 1:
                for f_item in f_list:
                    if Item[0] != f_item[0]: print("=> ", f_item)
            print()