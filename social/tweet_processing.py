import re
import nltk
import string
# from gensim import corpora, models, similarities
from nltk import wordpunct_tokenize # function to split up our words
from nltk.corpus import stopwords
# to optimise, we can just check for the DNS if < 3 to expand url
import requests
session = requests.Session()  # so connections are recycled
from bs4 import BeautifulSoup
# email
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# === Params ===

search = ['any', 'deep', 'learning']
search2 = ['any', 'robot', 'bot']
search3 = ['all', 'job', 'ai']
search5 = ['all', 'ai']
search4 = ['any', 'CES', 'ces', 'CES2017', 'ces 2017']
Team = False
Email = True

shorteners = ['tr.im','is.gd','tinyurl.com','bit.ly','snipurl.com', 'amzn.to',
              'cli.gs','feedproxy.google.com','feeds.arstechnica.com', 'flip.it',
              'buff.ly', 'adf.ly', 't.co', 'goo.gl', 'j.gs', 'dlvr.it',
              'bbc.in', 'fb.me', 'ift.tt', 'amzn.co', 'htn.to', 'hubs.ly',
              'j.mp', 'klou.tt', 'ln.is', 'ow.ly', 'odca.it', 'rickp.co', 'tcrn.ch',
              'usat.ly', 'zd.net', 'lnkd.in', 'tmblr.co', 'block.ly', 'cur.lv', 't.co',
              'j.obs.link', 'clz.es', 'zpr.io', 'cur.lv', 'ht.ly', 'nzy.ch', 'sec.tips',
              'sh.st', 'prt.news', 'onforb.es', 'okt.to', 'nzzl.us', 'owler.us', 'theo.re',
              'wpo.st', 'ubm.io', 'dailym.ai', 'link.thomas.do', 'netvib.es', 'wsj.com',
              'sociably.me', 'is.gd', 'sh.st', 'bnc.lt', 'apple.news', 'shar.es',
              'oxford.ly', 'bit.ly', 'dld.bz', 'ibm.biz', 'phin.ws', 'read.aici.al',
              'sco.lt', 'trap.it', 'upflow.co', 'v3.co.uk', 'cg.nu', 'wp.me', 'nyti.ms',
              'mtth.in', 'mhb.io', 'adobe.ly', 'po.st', 'tek.io', 'spr.ly', 'linkis.com',
              'upflow.co', 'disq.us', 'tek.io', 'zpy.io', 'read.aici.al', 'sbne.ws', 'e.gv.co']

blacklist = ['google', 'youtube', 'facebook', 'twitter', 'pinterest', 'ft.com', 'amazon.com', 'paper.li', 'cur.lv', 'boysofts.com', 'crowdfireapp.com']

# ==========

def stopwordsCustom():
    # Get default English stopwords and extend with punctuation
    stopwords = nltk.corpus.stopwords.words('english')
    stopwords.extend(string.punctuation)
    stopwords.append('')
    return stopwords

def get_language_likelihood(input_text):
    # Return a dictionary of languages and their likelihood of being the
    # natural language of the input text
    input_text = input_text.lower()
    input_words = wordpunct_tokenize(input_text)

    language_likelihood = {}
    total_matches = 0
    for language in stopwords._fileids:
        language_likelihood[language] = len(set(input_words) &
                set(stopwords.words(language)))

    return language_likelihood

def get_language(input_text):
    # Return the most likely language of the given text
    likelihoods = get_language_likelihood(input_text)
    return sorted(likelihoods, key=likelihoods.get, reverse=True)[0]

# ==========

def loadFeeds(fileName):
    docs = []
    with open(fileName) as word_file:
        for sent in word_file:
            try:
                docs.append(sent)
            except:
                pass
    return docs

def filter_language(documents_brut):
    documents_lngfilter = []

    # Filter based on the language
    for doc in documents_brut:
        # petite normalisation et split sur les |||
        doc = doc.replace("\n", "").split("|||")
        try:
            if get_language(doc[2]) == 'english' and len(doc) == 8:
                documents_lngfilter.append(doc)
        except:
            print(doc)

    # return documents_lngfilter
    return documents_lngfilter

def TraitFeedsList(myList, nbrSents=0):

    # List all items in the substract
    newList = []
    # lang we keep
    lang = ['en', 'sv']
    # loading the stopwords
    stopword = stopwordsCustom()

    # here is where we normalise the text
    for sent in myList:
        item=[]
        # validate we have more than 3 elements in our tweet information
        # validate the langage of from tweeter
        # validate the number of letters in the tweets
        if (len(sent) > 3) and (sent[1] in lang) and (len(sent[2]) > 10):
            # cleaning any url inside of the text
            temp = re.sub("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", '',
                      sent[2].lower())
            # cleaning any special caracters : TODO what about @ref and #ref ?
            temp = re.sub(r'[?|$|.|…|“|,|”|!|#|/|(|)|&|*|–|-|"|@|:|;|{|}|''|.]',r'',temp)
            # clearning the simple '
            temp = temp.strip().replace("'", "")
            # extract stopwords
            sent[2] = [word for word in temp.split(" ") if word not in stopword]
            sent[3] = sent[3].replace("\n", "")
            # create final item = [TweetID], [Tokens]
#             item.append(extract[0])
#             item.append(extract[2])
            newList.append(sent)

    # if we need to send an sample or all sentences
    if nbrSents == 0:
        return newList
    else:
        return newList[0:nbrSents]

def url_normalisation(url_brut):

    # TODO : etendre search avec vecteurs
    # TODO : inverser le text sorted avec la recherche !!!

    url_list=[]
    previous_url = " # "
    short_urls = []

    cSearch = search[0]
    search.remove(search[0])

    for item in url_brut:
        # HERE change 'all' with 'any'
        if cSearch == 'any':
            if any(x in item[2] for x in search):
                url = item[3]
                # Ici je verifie que l'url n'est pas deja existante
                # TODO : ici faire un multi conditionel sur les likes et retweets
                if previous_url in url:
                    # creation d'un dictionnaire d'URL temporaire
                    # ici on reprend la racine
                    pass
                else:
                    previous_url = url
                    # expansion of the url if in shorteners list : TODO create a learning system about shorteners list
                    # http://dig.do/about/url-shortener
                    if any(x in url for x in set(shorteners)):
                        short_urls.append(url)
                        try:
                            resp = session.head(url, allow_redirects=True)
                            # split based on the '?', replace some elements and add to list
                            # TODO : faire un split sur le #
                            item[3] = resp.url.split("?")[0].replace("%0A","")
                        except Exception as e:
                            item[3] = url
                            print(e)
                    else:
                        item[3] = url

                    # Add the new item url expanded to the list
                    url_list.append(item)
        elif cSearch =='all':
            if all(x in item[2] for x in search):
                url = item[3]
                # Ici je verifie que l'url n'est pas deja existante
                # TODO : ici faire un multi conditionel sur les likes et retweets
                if previous_url in url:
                    # creation d'un dictionnaire d'URL temporaire
                    # ici on reprend la racine
                    pass
                else:
                    previous_url = url
                    # expansion of the url if in shorteners list : TODO create a learning system about shorteners list
                    # http://dig.do/about/url-shortener
                    if any(x in url for x in set(shorteners)):
                        short_urls.append(url)
                        try:
                            resp = session.head(url, allow_redirects=True)
                            # split based on the '?', replace some elements and add to list
                            # TODO : faire un split sur le #
                            item[3] = resp.url.split("?")[0].replace("%0A","")
                        except Exception as e:
                            item[3] = url
                            print(e)
                    else:
                        item[3] = url

                    # Add the new item url expanded to the list
                    url_list.append(item)

    return url_list

def url_count(urls_list):

    # Dedup URL
    previous_url = " # "

    blacklist_count = 0

    text_filtered = []
    texts_search = sorted(urls_list, key=lambda x: x[3])

    count = 1
    for item in texts_search:
        url = item[3]
        # delete the last caracter of the string if = '/'
        if url.endswith("/"): url = url[:-1]
        # if the new url is not a root dns and not an extension of the previous url
        if previous_url in url and previous_url.count("/") > 2:
            count = count+1
            pass
        else:
            # if url is not in the blacklist or not a root dns
            if not any(x in url for x in set(blacklist)) and url.count("/") > 2:
                if len(text_filtered) > 0:
                    index = len(text_filtered) -1
                    text_filtered[index].append(count)
                count = 1
                previous_url = url
                text_filtered.append(item)
            else:
                blacklist_count = blacklist_count + 1

    text_filtered[len(text_filtered)-1].append(count)
    return text_filtered, blacklist_count

def summary(documents_brut, documents, texts, texts_search, text_filtered):
    summary = "Total Items : " + str(len(documents_brut)) + "\n"
    summary = summary + "Total Items English : " + str(len(documents)) + "\n"
    summary = summary + "Total Items English with valid content : " + str(len(texts)) + "\n"
    summary = summary + "Total Items After Search '" + ", ".join(search) + "' : " + str(len(texts_search)) + "\n"
    summary = summary + "Total Items Final : " + str(len(text_filtered)) + "\n"
    return summary

def extract_print_content_html(body, text_filtered):

    for item in text_filtered:
        url = item[3]
        page = []

        # load the title of the page
        try:
            html = requests.get(url)
            soup = BeautifulSoup(html.text, "html.parser")
            title = soup.title.string
        except Exception as e:
            title = e

    #     # load the meta description of the page
    #     md = ''
    #     l = soup.findAll("meta", attrs={"name":"description"})
    #     if l == []:
    #         # "No meta description"
    #         md = "na"
    #     else:
    #         md = l[0]['content'].encode('utf-8')

        result = []
        result.append(title)
    #     result.append(md)
        result.append(item[3])

        # add the item to the collection
        page.append(result)

    #     # load the meta keywords of the page
    #     mk = ''
    #     l = soup.findAll("meta", attrs={"name":"keywords"})
    #     if l == []:
    #         mk = "No meta keywords"
    #     else:
    #         mk = l[0]['content'].encode('utf-8')
    #     #page.append(mk)

    #     # load the meta robots of the page
    #     mr = ''
    #     l = soup.findAll("meta", attrs={"name":"robots"})
    #     if l == []:
    #         mr = "No meta robots"
    #     else:
    #         mr = l[0]['content'].encode('utf-8')
    #     #page.append(mr)

        # page.append(soup.findAll("meta", attrs={"name":"description"})[0]['content'].encode('utf-8'))
        for item in page:
            # print Title
            print(str(item[0]).strip())
            body = body + (str(item[0]).strip()) + "\n"
            # print description
    #         if item[2] != "na" and len(item[2].split(" ")) > 5 : print(item[2].strip())
            # print URL
            print(item[1])
            body = body + item[1] + "\n"
            # print space
            print()

    return body

def send_email(body="Body", title="Title", Team = False):

    sender = "tobswen.ai@gmail.com"
    password = "tobswen123;"

    if Team == True :
        recipients = ["gregrenard@gmail.com"]
        # recipients = ["gregrenard@gmail.com", "guillaumedominici@gmail.com", "louis.monier@gmail.com", "audrey@duetcahn.com", "mathias.herbaux@viacesi.fr", "gregory.senay@gmail.com"]
    else:
        recipients = ["gregrenard@gmail.com"]

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    # msg['Subject'] = "Collecte " + title
    msg['Subject'] = "Collecte " + dateCollecte + " : " + str(search)

    msg.attach(MIMEText(body, 'plain'))

    try:
        with smtplib.SMTP('smtp.gmail.com', 587) as server:
            server.ehlo()
            server.starttls()
            server.ehlo()
            server.login(sender, password)
            server.sendmail(sender, recipients, msg.as_string())
            server.close()
            # server.quit()
        print("Email Sent!")
    except:
        print("Unable to send email!")
        raise

# if len(texts_search) > 0: print(texts_search[0])
# for doc in sorted(texts_search, key=lambda x: x[3]):
#     print(doc[3])

# === Tweets Processing Area ===

def Tweets_Processing_email(dateCollecte):

    try:
        # chargement des tweets
        documents_brut = loadFeeds(dateCollecte)
        # Gestion du language
        documents = filter_language(documents_brut)
        # Normalisation of the tweets list
        texts = TraitFeedsList(documents)
        # Normalisation of URLs
        texts_search = url_normalisation(texts)
        # count des urls
        text_filtered, blacklist_count = url_count(texts_search)

        # === email area ===
        body = summary(documents_brut, documents, texts, texts_search, text_filtered)
        body = extract_print_content_html(body, text_filtered)
        if Email: send_email(body, "", True)
    except:
        print("Unable to process data!")
        raise

def Tweets_Processing_email_Print(dateCollecte):

    # chargement des tweets
    documents_brut = loadFeeds(dateCollecte)
    # Gestion du language
    documents = filter_language(documents_brut)
    # Normalisation of the tweets list
    texts = TraitFeedsList(documents)
    # Normalisation of URLs
    texts_search = url_normalisation(texts)
    # count des urls
    text_filtered, blacklist_count = url_count(texts_search)

        # === email area ===
    body = summary(documents_brut, documents, texts, texts_search, text_filtered)
    body = extract_print_content_html(body, text_filtered)
    if Email: send_email(body)

    # === Print Area ===

    print("Total Items : ", len(documents_brut))
    print("Total Items English : ", len(documents))
    print("Total Items English with valid content : ", len(texts))
    # print(len(texts_sorted))
    print("Total Items After Search ", len(texts_search))
    print("Total Items Final : ", len(text_filtered))
    print("Total Bladlist Item", blacklist_count)

    Total = 0
    text_filtered = sorted(text_filtered, key=lambda x: -x[8])
    for item in text_filtered:
        print(item[8], " - ", item[3])
        Total = Total + int(item[8])

    # print(len(text_filtered))
    # print(Total+blacklist_count)

# send_email("email body", "email title", True)

# === Default ===
from _datetime import datetime
import time

while True:
    d = datetime.now()
    if d.hour >= 20 and d.minute >= 00:
        # dateCollecte = "FeedTwitter-2016_09_13.txt"
        dateCollecte = "FeedTwitter-" + datetime.now().strftime('%Y_%m_%d') + ".txt"
        print(dateCollecte)
        Tweets_Processing_email(dateCollecte)

        # Gros Hack pour generer le second
        search = search2
        Tweets_Processing_email(dateCollecte)

        # Gros Hack pour generer le troisieme
        search = search3
        Tweets_Processing_email(dateCollecte)

        # Gros Hack pour generer le troisieme
        search = search4
        Tweets_Processing_email(dateCollecte)

        # Gros Hack pour generer le troisieme
        search = search5
        Tweets_Processing_email(dateCollecte)

        # while d.hour >= 20:
        #    d = datetime.now()
        search = ['any', 'deep', 'learning']
        time.sleep(14400)
    else:
        print("Hold : ", d)
        time.sleep(60)
