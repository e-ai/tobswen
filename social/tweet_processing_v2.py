import re
import nltk
import string
from nltk import wordpunct_tokenize # function to split up our words
from nltk.corpus import stopwords
# to optimise, we can just check for the DNS if < 3 to expand url
import requests
session = requests.Session()  # so connections are recycled
from bs4 import BeautifulSoup
# email
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from _datetime import datetime

# === Params ===

blacklist = ['google', 'youtube', 'facebook', 'twitter', 'pinterest', 'ft.com', 'amazon.com', 'paper.li', 'cur.lv', 'boysofts.com', 'crowdfireapp.com']
# dateCollecte = "../../Tobswen/social/FeedTwitter-" + datetime.now().strftime('%Y_%m_%d') + ".txt"
dateCollecte = "FeedTwitter-2016_11_01.txt"

# ====================

# Loading the file with URLs
def loadFile(File):
    with open(File) as urls_file:
        return set((url.strip().lower() for url in urls_file))

# Saving file
def SaveInFile(File, Content):
    output1 = open(File,"a")
    output1.write(Content)
#     output1.write('\n')
    output1.close()

def stopwordsCustom():
    # Get default English stopwords and extend with punctuation
    stopwords = nltk.corpus.stopwords.words('english')
    stopwords.extend(string.punctuation)
    stopwords.append('')
    return stopwords

def loadFeeds(fileName):
    docs = []
    with open(fileName) as word_file:
        for sent in word_file:
            try:
                docs.append(sent)
            except:
                pass
    return docs

def get_language_likelihood(input_text):
    # Return a dictionary of languages and their likelihood of being the
    # natural language of the input text
    input_text = input_text.lower()
    input_words = wordpunct_tokenize(input_text)

    language_likelihood = {}
    total_matches = 0
    for language in stopwords._fileids:
        language_likelihood[language] = len(set(input_words) &
                set(stopwords.words(language)))

    return language_likelihood

def get_language(input_text):
    # Return the most likely language of the given text
    likelihoods = get_language_likelihood(input_text)
    return sorted(likelihoods, key=likelihoods.get, reverse=True)[0]

def filter_language(documents_brut):
    documents_lngfilter = []

    # Filter based on the language
    for doc in documents_brut:
        # petite normalisation et split sur les |||
        doc = doc.replace("\n", "").split("|||")
        try:
            if get_language(doc[2]) == 'english':
                documents_lngfilter.append(doc)
        except:
            print(doc)

    # return documents_lngfilter
    return documents_lngfilter

# normalisation 1 : lang, stopwords, urls extracts from text, replace \n
def TraitFeedsList(myList, nbrSents=0):

    # List all items in the substract
    newList = []
    # lang we keep
    lang = ['en', 'sv']
    # loading the stopwords
    stopword = stopwordsCustom()

    # here is where we normalise the text
    for sent in myList:
        item=[]
        # validate we have more than 3 elements in our tweet information
        # validate the langage of from tweeter
        # validate the number of letters in the tweets
        if len(sent) > 3 and sent[1] in lang and len(sent[2]) > 10:
            # cleaning any url inside of the text
            temp = re.sub("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", '',
                      sent[2].lower())
            # cleaning any special caracters : TODO what about @ref and #ref ?
            temp = re.sub(r'[?|$|.|…|“|,|”|!|/|(|)|&|*|–|-|"|@|:|;|{|}|''|.]',r'',temp)
            # cleaning the simple '
            temp = temp.strip().replace("'", "")
            # extract stopwords
            sent[2] = [word for word in temp.split(" ") if word not in stopword]
            sent[3] = sent[3].replace("\n", "")
            # create final item = [TweetID], [Tokens]
#             item.append(extract[0])
#             item.append(extract[2])
            newList.append(sent)

    # if we need to send an sample or all sentences
    if nbrSents == 0:
        return newList
    else:
        return newList[0:nbrSents]

# OBSOLETE ??? : Where we are doing the search
def url_normalisation(url_brut):

    # TODO : etendre search avec vecteurs
    # TODO : inverser le text sorted avec la recherche !!!

    url_list=[]
    previous_url = " # "
    short_urls = []

    for item in url_brut:
        # SEARCH => change 'all' with 'any'
        if any(x in item[2] for x in search):
            # DUPLICATE URL Mgt
            url = item[3]
            # Ici je verifie que l'url n'est pas deja existante
            # TODO : ici faire un multi conditionel sur les likes et retweets
            if previous_url in url:
                # creation d'un dictionnaire d'URL temporaire
                # ici on reprend la racine
                pass
            else:
                previous_url = url
                # expansion of the url if in shorteners list : TODO create a learning system about shorteners list
                # http://dig.do/about/url-shortener
                if any(x in url for x in set(shorteners)):
                    # Temporary disctionnary of URLS : To bo use !!!
                    short_urls.append(url)
                    try:
                        # URL Expansion with session
#                         resp = session.head(url, allow_redirects=True)
                        # URL Expansion with requests
                        resp = requests.get(url, allow_redirects=True)
                        # split based on the '?', replace some elements and add to list
                        # TODO : faire un split sur le #
                        item[3] = resp.url.split("?")[0].replace("%0A","")
                    except Exception as e:
                        item[3] = url
                        print(e)
                else:
                    item[3] = url

                # Add the new item url expanded to the list
                url_list.append(item)

    return url_list

# url expansion, content collect
def url_expansion(urls_brut):
    # Order based on URLs
    urls_brut = sorted(urls_brut, key=lambda x: x[3])

    # Dedupe based on URLs
    urls_brut_dedup = []
    seen = {}
    # for item in tqdm_notebook(urls_brut, desc="Dedup URLs"):
    for item in urls_brut:
        if item[3] not in seen:
            seen.setdefault(item[3], item[3])
            urls_brut_dedup.append(item)
        else:
            pass

    # URLs Expansion
    urls_expanded = []
    for item in urls_brut_dedup:
        try :
            r = requests.get(item[3], timeout=5, allow_redirects=True)

            # === Page URL ===
            url = r.url.split("?")[0].replace("%0A","")

            # === Page TITLE ===

            try:
                soup = BeautifulSoup(r.text, "html.parser")
                title = soup.title.string
            except Exception as e:
                title = "empty"

            # === Page Description ===

            md = ''
            l = soup.findAll("meta", attrs={"name":"description"})
            if l == []:
                # "No meta description"
                md = "No description"
            else:
                md = l[0]['content'].encode('utf-8')

            # === Page Meta Keywords ===
            mk = ''
            l = soup.findAll("meta", attrs={"name":"keywords"})
            if l == []:
                mk = "No meta keywords"
            else:
                mk = l[0]['content'].encode('utf-8')

            # === Page Meta Robots ===
            mr = ''
            l = soup.findAll("meta", attrs={"name":"robots"})
            if l == []:
                mr = "No meta robots"
            else:
                mr = l[0]['content'].encode('utf-8')

            # LIST update url and add title
            item[3] = url
            item.append(title)
            item.append(md)
            item.append(mk)
            item.append(mr)

            urls_expanded.append(item)

        except:
            print("error url expansion + collect")
            pass

    # Order based on URLs expanded
    urls_expanded = sorted(urls_expanded, key=lambda x: x[3])

    # Dedupe based on URLs expanded
    urls_expanded_dedup = []
    seen = {}
    for item in urls_expanded:
        if item[3] not in seen:
            seen.setdefault(item[3], item[3])
            urls_expanded_dedup.append(item)
        else:
            pass

    return urls_expanded_dedup

def url_count(urls_list):

    # Dedup URL
    previous_url = " # "

    blacklist_count = 0

    text_filtered = []
    texts_search = sorted(urls_list, key=lambda x: x[3])

    count = 1
    for item in texts_search:
        url = item[3]
        # delete the last caracter of the string if = '/'
        if url.endswith("/"): url = url[:-1]
        # if the new url is not a root dns and not an extension of the previous url
        if previous_url in url and previous_url.count("/") > 2:
            count = count+1
            pass
        else:
            # if url is not in the blacklist or not a root dns
            if not any(x in url for x in set(blacklist)) and url.count("/") > 2:
                if len(text_filtered) > 0:
                    index = len(text_filtered) -1
                    text_filtered[index].append(count)
                count = 1
                previous_url = url
                text_filtered.append(item)
            else:
                blacklist_count = blacklist_count + 1

    text_filtered[len(text_filtered)-1].append(count)
    return text_filtered, blacklist_count

# ====================

# shorteners = loadFile("shortURLs.txt")

search = ['deep', 'learning']
search2 = ['robot', 'bot']
search3 = ['ai']

# chargement des tweets
documents_brut = loadFeeds(dateCollecte)
# Gestion du language
documents = filter_language(documents_brut)
# Normalisation of the tweets list
texts = TraitFeedsList(documents)
# expansion of the urls list and web page collect
results = url_expansion(texts)

# Search Filter
item_filter = []
for item in results:
    if any(x in item[2] for x in search):
        item_filter.append(item)

# Order based on Title
item_filter = sorted(item_filter, key=lambda x: x[8])

# Dedupe based on Title
item_title_dedup = []
seen = {}
for item in item_filter:
    if item[8] not in seen:
        seen.setdefault(item[8], item[8])
        item_title_dedup.append(item)
    else:
        pass

print(len(documents_brut))
print(len(documents))
print(len(texts))
print(len(results))
print(len(item_filter))
print(len(item_title_dedup))

# Final Print
for item in item_title_dedup:
    # 8 = Title | 3 = URL | 9 = Description
    print(item[8].replace("\n", "").strip())
    print(str(item[9]).replace("b",""))
    print(item[3])
    print()