import tweepy
import json
from tweepy import Stream
from tweepy import OAuthHandler
from keys import keys

# === consumer key, consumer secret, access token, access secret.
ckey=keys['consumer_key']
csecret=keys['consumer_secret']
atoken=keys['access_token']
asecret=keys['access_token_secret']

# === Authentification ===
auth = OAuthHandler(ckey, csecret)
auth.set_access_token(atoken, asecret)

api = tweepy.API(auth)

query = 'Lille #news'

max_tweets = 100
searched_tweets = tweepy.Cursor(api.search, lang='en', q=query).items(max_tweets)

for item in searched_tweets:
    tweet = item.text
    count = 0
    if tweet.find('RT') == -1 and tweet.find('http') > -1:
        count = count+1
        # print(item)
        print(item.text)

print(count)