
# coding: utf-8

# In[ ]:

import logging
from pymongo import MongoClient
from pymongo.errors import CursorNotFound
import html2text
from langdetect import detect
import re
from html import unescape
from html.parser import HTMLParserError
from nlumtp.preprocessors.cleaners.whitelist import CleanChars
import time
import pandas as pd
from collections import Counter
from nltk.corpus import stopwords
from operator import itemgetter
from tqdm import tqdm
from newspaper import Article
from nltk.tokenize import wordpunct_tokenize
from utils import csv_to_list_dict, only_id


# In[ ]:

# mongo collections
client = MongoClient()
db = client.search_subject_v3
real_urls_collection = db.real_urls


# In[ ]:

# html2text configuration
text_maker = html2text.HTML2Text()
text_maker.ignore_links = True
text_maker.body_width = 0
text_maker.ignore_images = True
text_maker.ignore_emphasis = True
text_maker.ignore_links = True
text_maker.ignore_tables = True
text_maker.mark_code = False

cc = CleanChars.only_alphanum_cleaner()

# text extractor params (determines which lines we keep)
min_words = 20  # how much words does a line need to survive content filtering
handicap_factor = 2.5  # if a line is between 2 lines shorter than min_words it has to be longer to be kept
boost_factor = 0.6  # if a line is between 2 lines longer than min_words it can be kept despite being shorter
use_newspaper = False  # we can use newspaper instead of our custom process, but it's not advised (here for the record)

# params for ngram extraction
tfidf_limit = 50  # how much words/ngrams do we keep in mongodb
max_ngram_len = 8  # we look for ngrams with less than this much words only
allowed_languages = [('fr', 'french'), ('en', 'english')]  # because we need different ngrams dict for each lang.

# params for content comparison
min_paragraph_length = 50  # min number of char for a line to be considered has an entry of text comparison
min_total_len = 400  # min length for the final extracted content to be considered in the next steps

sw = set(stopwords.words('french') + stopwords.words('english'))
punct = set(",;.?:!")


# In[ ]:

save_to_files = True  # the content is also saved into mongodb. True for now but probably useless
raw_root_folder='datas/raw_extracted/'  # where to save result from html2text
txt_root_folder='datas/extracted/'  # where to save filtered content but before cleaning
clean_root_folder='datas/cleaned/'  # where to save final, cleaned, content


# In[ ]:

# load ngrams and words dictionaries and idf weights
idf_dict = {}
idf_dict_ng = {}
for lang, lang_long in allowed_languages:
    idf_dict[lang] = csv_to_list_dict("../../datasets/wiki-idf-count/idf-dict-wiki-" + lang + "-nosw.csv")
    idf_dict_ng[lang] = csv_to_list_dict("../../datasets/wiki-idf-count/idf-dict-wiki-ngrams-" + lang + "-nosw.csv")


# In[ ]:

# some heuristics to determine if a line is actual content or boilerplate, code, ad...
def keep_line(s):
    s = s.strip().lower()
    if len(s) <= 5:
        return False
    if s.endswith("..."):
        return False
    if s.endswith("…"):
        return False
    if s.endswith("read more"):
        return False
    if "read more" in s[-12:]:
        return False
    ratio = s.count(' ') / len(s)
    if ratio < 0.1 or ratio > 0.25:
        return False
    
    words = wordpunct_tokenize(s)
    ratio_sw = sum(w in sw for w in words) / len(words)
    if ratio_sw < 0.1:
        return False
    
    ratio_digits = sum(c.isdigit() for c in s) / len(s)
    if ratio_digits > 0.1:
        return False
        
    nb_code = sum(1 for x in s if x in {'{', '}', '\\'})
    if nb_code >= 5: # probably code
        return False
    
    nb_punct = sum(c in punct for c in s)
    if len(s) > 100 and nb_punct == 0:
        return False
    if nb_punct / len(s) > 0.1:
        return False
    
    return True


# In[ ]:

def extract_from_html(html_content):
    
    if use_newspaper:  # try to extract content with newspaper
        art = Article("http://abc.de") # unused url, but we get an error if it is invalid...
        art.download(html=html_content)
        art.parse()
        title = art.title
        filtered_content = art.text
    
    if not use_newspaper or len(title) < 3:
        try:
            title = re.search(r'\<title.*?\>(.*?)\<\/title\>', unescape(html_content), flags=re.DOTALL).group(1)
            if len(title) > 100:
                title = title[:100] + "..."
        except:
            title = None

    try:
        md_content = unescape(text_maker.handle(html_content))  # THIS IS SOMETIMES VERY SLOW. WHY?
    except HTMLParseError as error:
        md_content = None
        logging.exception(error)

    if md_content is not None and not use_newspaper or len(filtered_content) < 3:  # ie. if
        # newspaper disabled or enabled
        # but failed extraction
        first_pass = [len(s.split()) for s in md_content.split('\n') if keep_line(s)]  # first remove invalid lines

        fp_index = 0
        interesting_sentences = []
        for s in md_content.split('\n'):
            s = s.strip()
            if not keep_line(s): continue
            
            # then, in the remaining text, remove short lines (with handicap/boost)
            if (fp_index == 0 or fp_index == len(first_pass) - 1):
                if first_pass[fp_index] > min_words:
                    interesting_sentences.append(s)
            elif first_pass[fp_index] > min_words and (first_pass[fp_index - 1] > min_words or first_pass[fp_index + 1] > min_words):
                interesting_sentences.append(s)
            elif first_pass[fp_index] > min_words * handicap_factor and first_pass[fp_index - 1] <= min_words and first_pass[fp_index + 1] <= min_words:
                interesting_sentences.append(s)
            elif first_pass[fp_index] > min_words * boost_factor and (first_pass[fp_index - 1] > min_words * handicap_factor or first_pass[fp_index + 1] > min_words * handicap_factor) and (first_pass[fp_index - 1] > min_words or first_pass[fp_index + 1] > min_words):
                interesting_sentences.append(s)
            # else: interesting_sentences.append("XXXXXX\t\t" + s)
            fp_index += 1

        filtered_content = "\n\n".join(interesting_sentences)
    try:
        lang = detect(filtered_content)
    except:
        lang = None
    
    clean_content = cc.preprocess_string_dataset(filtered_content)
    clean_lines = clean_content.split('\n')
    doublons = Counter([s[:40] for s in clean_lines if len(s) > 10])
    forbidden_prefix = {s for s in doublons if doublons[s] > 2}  # if text is there >= 3 times, it's probably boilerplate
    seen_lines = set()
    
    # if text is here exactly twice we want to keep it, but once
    no_doublons_clean_content = '\n'.join(s for s in clean_lines if s[:40] not in forbidden_prefix and not (s in seen_lines or seen_lines.add(s)))
    
    return lang, title, no_doublons_clean_content, filtered_content, md_content


# In[ ]:

# call the previous function and handle io with mongo and files
def extract_content(url, apply=False):
    txt_path = txt_root_folder + url["hash"] + '.txt'
    raw_txt_path = raw_root_folder + url["hash"] + '.txt'
    clean_txt_path = clean_root_folder + url["hash"] + '.txt'
    
    with open(txt_path, 'w') as dst, open(raw_txt_path, 'w') as raw, open(clean_txt_path, "w") as cln:
        html_content = url["content_html"]
        
        lang, title, clean_content, content, raw_content = extract_from_html(html_content)
        
        if save_to_files:
            content_error = None
            try:
                dst.write(url["url"] + "\n\n\n\n" + content)
            except UnicodeEncodeError:
                # TODO : check content, see if we can't find a work around
                content_error = "UnicodeEncodeError exception raised"
                content  = content_error
            try:
                raw.write(url["url"] + "\n\n\n\n" + raw_content)
            except UnicodeEncodeError:
                raw_content = "UnicodeEncodeError exception raised"

            try:
                cln.write(url["url"] + "\n\n\n\n" + clean_content)
            except UnicodeEncodeError:
                content_error = "UnicodeEncodeError exception raised"
                clean_content  = content_error
        
        set_params = {
                        "html_extr_version": last_html_extr_version,
                        "txt_len": len(clean_content), 
                        "txt_lang": lang, 
                        "txt_title": title,
                        "content_clean": clean_content,
                        "content_text": content,
                        "content_raw_text": raw_content,
                    }
        if len(content) < 2:
            set_params["error"] = "no content extracted"
        elif content_error is not None:
            set_params["error"] = content_error
        
        if apply:
            real_urls_collection.update_one(only_id(url), {"$set": set_params})
        else:
            print("Warning: not applied")
        return set_params


# In[ ]:

# once we have extracted a clean text, we want to get the most important words/ngrams
def get_tfidf(s, lang):
    words = s.split()
    word_counter = Counter(words)
    
    if lang not in [l for l, _ in allowed_languages]:
        return None, None, None, word_counter.most_common(tfidf_limit), None
    
    # condensed way of counting all occurrences of ngrams of any length between 2 and max_ngram_len words in text.
    ngram_counter = Counter()
    for n in range(2, max_ngram_len):
        ngram_counter.update([' '.join(x) for x in zip(*[words[k:-(n-k)] for k in range(n)]) if ' '.join(x) in idf_dict_ng[lang]])
    
    word_tfidf = Counter({k: word_counter[k] * idf_dict[lang][k] for k in word_counter}).most_common(tfidf_limit)
    ngram_tfidf = Counter({k: ngram_counter[k] * idf_dict_ng[lang][k] for k in ngram_counter}).most_common(tfidf_limit)
    oov = {k: word_counter[k] for k in word_counter if k not in idf_dict[lang]}
    
    return word_tfidf, ngram_tfidf, oov, word_counter.most_common(tfidf_limit), ngram_counter.most_common(tfidf_limit)


# In[ ]:

# save the beginning and length of lines for comparison in the next process
def get_pdict(text):
    page_dict = {}
    for line in text.split("\n"):
        line = line.strip()
        line_len = len(line)
        
        if line_len < min_paragraph_length: continue

        prefix = line[0:40]
        page_dict[prefix] = line_len

    return page_dict


# In[ ]:

# call the previous two functions and handle io with mongo
def extract_ngrams(url, apply=False):
        lang = url["txt_lang"]
        clean_content = url["content_clean"]
        
        word_tfidf, ngram_tfidf, oov, word_counter, ngram_counter = get_tfidf(clean_content, lang)

        set_params = {"ngrams_extr_version": last_ngram_extr_version,}
        
        set_params["most_common_words"] = [{"name": x[0], "count": x[1]} for x in word_counter]
        
        if word_tfidf is None:
            set_params["error"] = "language not supported for idf"
        else:
            set_params["most_common_ngrams"] = [{"name": x[0], "count": x[1]} for x in ngram_counter]
            set_params["idf_word"] = [{"name": x[0], "tfidf": x[1]} for x in word_tfidf]
            set_params["idf_ngram"] = [{"name": x[0], "tfidf": x[1]} for x in ngram_tfidf]
        
        set_params["page_dict"] = get_pdict(clean_content)
        set_params["good_for_clusters"] = len(clean_content) > min_total_len
        
        if apply:
            real_urls_collection.update_one(only_id(url), {"$set": set_params})
        else:
            print("Warning: not applied")
            
        return set_params


# In[ ]:

# the whole processing of an url
def extract_both(url, apply=False):
    content_updated = False
    if url.get("html_extr_version", -1) < last_html_extr_version:
        new_params = extract_content(url, apply=apply)
        for k in new_params:
            url[k] = new_params[k]
        content_updated = True
    if url.get("ngrams_extr_version", -1) < last_ngram_extr_version or content_updated:
        new_params = extract_ngrams(url, apply=apply)
        for k in new_params:
            url[k] = new_params[k]
    
    return url


# In[ ]:

# wrapper to run a function on a collection with a given mongo query
# set continuous to True if the process must listen forever to the url queue
def run_all(coll, fun, pool, continuous=False):
    done = False
    while not done or continuous:
        nb_urls = coll.count(pool)
        if nb_urls == 0:
            print("nothing to do...")
            time.sleep(10)
        else:
            try:
                for entry in tqdm(coll.find(pool), total=nb_urls):    
                    fun(entry, apply=True)
            except CursorNotFound:
                pass
        done = True


# In[ ]:

# update those values when making a change to the extraction methods so the already processed content is re-extracted 
# (only affect touched real_urls, not the whole history)
last_html_extr_version = 3
last_ngram_extr_version = 2


# In[ ]:

# partially determines if an url should be considered for extraction
cond_extr = [{"html_extr_version": {"$lt": last_html_extr_version}}, {"html_extr_version": {"$exists": False}}]
cond_ngram = [{"ngrams_extr_version": {"$lt": last_ngram_extr_version}}, {"ngrams_extr_version": {"$exists": False}}]


# In[ ]:

# old/debug code
# run_all(real_urls_collection, extract_content, {"$or": cond_extr})
# run_all(real_urls_collection, extract_ngrams, {"$or": cond_ngram})


# In[ ]:

# the actual function call. Run extraction on url with conditions above + if they have content and have been touched
run_all(real_urls_collection, extract_both, 
        {"$or": cond_extr + cond_ngram, "content_html": {"$exists": True}, "touched": True}, 
        continuous=True)

