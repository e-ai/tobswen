## Notes

A config file (`config.json`) is included in the repository in order to edit QUERIES and SEARCHES and adjust other params without editing the code.

Note that you don't need to quit/restart the processes to edit the config file, they will automatically reload it. 

After editing the configuration file, please check if an error is displayed in a process output: if the configuration is not valid anymore, the processes will keep using the older, working, version (so they won't crash) and display a warning.


## Full Description

collect.py:

- **owner_screen_name:** *string*, mostly used by QUERIES with lists (should be linked to the Twitter credentials)
- **run_every_in_minutes:** *int*, how often the collect script is run
- **queries:** *array*, a list of QUERIES. Format of each element:
    - **id:**, *string*, a unique identifier for the QUERY
    - **keywords:**, *string*, the query sent to Twitter API. Support a lot of operators such as AND, OR, filters... see complete doc: https://dev.twitter.com/rest/public/search
    - **sources:**, *array (optional, default: ["search"])*, should we collect from Twitter's search API or lists? (or both)

query-urls.py:

- **rerun_searches_hours_history:** *int*, when a SEARCH is created/edited in this config file, some urls may match SEARCHES they didn't before. So, we should rerun the SEARCHES on urls seen before, but not the whole history, as thousands of urls are fetched each day. This params specify how far we go back in urls history.
- **searches:** *array*, a list of SEARCHES. Format of each element:
    - **id:** *string*, unique identifier
    - **description** *string*, used for the title of the mail report
    - **queries:** *array*, limit the pool of urls considered for this report to those found in those queries. For each element, give the `id` and `sources` (optional, defaults to same as QUERY, and can be a subset of the actual QUERY's sources)
    - **vocab**: *array*, serves as a filter: only keeps the tweets containing at least one element of the array (ie. OR condition). An element can be either a string or an array of string (in this case, all elements of this array must be present - ie. AND condition). Ex: `[["recurrent", "network"], "rnn"]` is equivalent to `(recurrent AND network) OR rnn` and will match "recurrent neural network" and "char rnn" but not "business network".
    - **mails:** *array*, who should receive this report
    - **langs:** *array*, allowed languages
    - **top_n:** *int (optional)*, how much link should be displayed in the report

quasi-equal-deduper.py:

- **clusters_persistence_in_days:** *int*, when a new url is found, we try to associate it with a cluster. However, we can't compare it with every cluster in history, so we limit to the ones with activity in the last `clusters_persistence_in_days` days

get-stats.py:

- **default_top_n:** *int*, how much results at most should be displayed in the mail report (default value in case the `top_n` param is not specified in the SEARCH)
- **score_params:** *dict*, for each element, the `factor` attribute define how much this element weights in the popularity score of the url or content. The `max` attribute is a cap to reach to make the maximal score, further popularity doesn't count. Ex: with the rule `"favorite_count": {"factor": 2, "max": 100},` if a content has been favorited 100 or 300 times, in either case, this indicator will add 2 to the popularity score of the content.
- **nsfw_words:** *array*, a list of words that excludes a content from a report if at least one is in its title
- **nsfw_mails:** *array*, a report containing the contents filtered by the NSFW filter is sent to these emails (for debugging purpose)

## Example config file

```javascript
{
	"owner_screen_name": "gggdomi",
	"run_every_in_minutes": 5,
	"default_top_n": 20,
	"rerun_searches_hours_history": 48,
	"clusters_persistence_in_days": 3,
    "nsfw_mails": ["gregrenard@gmail.com", "guillaumedominici@gmail.com"],
	"all_mails_for_reference_only_not_used_in_code": 
	["gregrenard@gmail.com", "guillaumedominici@gmail.com", "louis.monier@gmail.com", "audrey@duetcahn.com", "mathias.herbaux@viacesi.fr", "gregory.senay@gmail.com"],

    "queries": [
    	{"id": "ces2017", "keywords": "ces2017 filter:links", "sources": ["search", "list"]},
    	{"id": "deep learning", "keywords": "deep learning filter:links", "sources": ["search"]}
    ],

    "searches": [{
    	"id": "deep learning",
    	"description": "Deep Learning (General)",
    	"queries": [
			{"id": "deep learning", "sources": ["search"]}
		],
		"vocab": [["deep", "learning"], "DL"],
		"mails": ["gregrenard@gmail.com", "guillaumedominici@gmail.com", "louis.monier@gmail.com", "mathias.herbaux@viacesi.fr", "patrick.mathieu@patrickmathieu.net", "aurelien.verla@gmail.com","oraffard@gmail.com"],
		"langs": ["en", "fr"],
		"top_n": 2500
	},{
    	"id": "CES AI",
    	"description": "AI at CES2017",
    	"queries": [
			{"id": "ces2017", "sources": ["search"]}
		],
		"vocab": ["ai", ["artificial", "intelligence"], "ia", ["intelligence", "artificielle"]],
		"mails": ["gregrenard@gmail.com", "guillaumedominici@gmail.com", "louis.monier@gmail.com", "audrey@duetcahn.com", "mathias.herbaux@viacesi.fr", "patrick.mathieu@patrickmathieu.net", "aurelien.verla@gmail.com", "oraffard@gmail.com"],
		"langs": ["en", "fr"],
		"top_n": 1500
	},{
    	"id": "CES cars",
    	"description": "Cars at CES2017",
    	"queries": [
			{"id": "ces2017", "sources": ["search"]}
		],
		"vocab": ["car", ["self", "driving"], ["conduite", "autonome"], "voiture", "truck", "uber"],
		"mails": ["gregrenard@gmail.com", "guillaumedominici@gmail.com", "louis.monier@gmail.com", "audrey@duetcahn.com", "mathias.herbaux@viacesi.fr", "patrick.mathieu@patrickmathieu.net", "aurelien.verla@gmail.com", "oraffard@gmail.com"],
		"langs": ["en", "fr"],
		"top_n": 1500
	}],

	"score_params": {
	    "friends_count": {"factor": 1, "max": 100000}, 
	    "followers_count": {"factor": 1, "max": 100000}, 
	    "retweet_count": {"factor": 1, "max": 1000},
	    "favorite_count": {"factor": 1, "max": 100},
	    "nb_tweets": {"factor": 1, "max": 100}
	},
    "nsfw_words": ["anal","analsex","asshat","asshole","biatch","bitch","bitching","blowjob","boobjob","boobs","c0ck","chink","clit","cock","cocks","cocksmoke","cocksuck","cooch","cum","cumshot","cunt","dick","dike","fuck","gangbang","jizz","masturbate","penis","porn","porno","prick","pussy","rimjob","semen","slut","sperm","spooge","suck","throttle","throttling","tit","titjob","tits","titties","tittys","vagina","whore","whorebag"]
}
```