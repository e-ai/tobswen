## Overview

There are five different mongodb collections used by the processes:

- **statuses:** Represent a tweet. Mainly the json from a tweet + some metadata
- **posters:** Represent a Twitter user. Mainly the user json from Twitter that comes with each status + some metadata. For now, only used for Twitter lists creation.
- **urls:** Represent a link found in a status. We don't know the "real" url after redirections before fetching it.
- **real_urls:** Represent a webpage at a given url (after redirection - many shortened urls can link to a single real url). Also store html and text contents.
- **contents:** Represent a content (text) possibly available on multilple pages and urls with small differences. It's a cluster of real_urls grouped by similar content.

## Full description

Here are all the attributes of the collections.

### - statuses

(mostly used by collect.py:)

- **tweet:** *dict*, the json from Twitter
- **tweet_download_date:** *date*, the last time we fetched the tweet (might be important if we want up-to-date popularity scores)
- **clean_text:** *string*, the text of the tweet, after normalization
- **tweet_language:** *string*, the language of the tweet, according to Twitter's api
- **is_retweet:** *boolean*, self-explanatory
- **sources:** *array* (should probably be renamed "queries"). List of the QUERIES (as in the config file) that brought back the tweet. Each element of the list is a dictionary:
	- **source:** *string*, "search" or "list"
	- **search_query:** *string*, the id of the query 
	- **list.name, list.owner:** *strings*, name and owner of the list (only if `source == list`)

### - posters

(mostly used by collect.py:)

- **twitter_id:** *int*
- **twitter_user:** *dict*, the json from Twitter
- **last_download_date:** *date*
- **search_queries:** *array*, the ids of the QUERIES were we found tweets by this user
- **nb_tweets_fetched:** *int*, how much tweets from this user has been encountered by our bot
- **original_poster:** *bool*, equal to True if we encountered at least one tweet by this user that was not a retweet
- **added_to_list:** *array*, the names of the lists where the user belongs

### - urls

(mostly used by collect.py:)

- **expanded_url:** *string*, the url found in the tweet
- **touched:** *boolean*, roughly states if the url is in the query-urls.py queue
- **clean_texts:** *array*, all the different texts of the tweets linking to this url
- **tweets_id:** *array*, the tweets linking to this url
- **sources:** *array*, the union of sources of all tweets linking to this url
- **creation_date:** *date*, when the url was first seen **(unused as of today)**

(mostly used by query-urls.py:)

- **state:** *string*, state of the request **(ex values: "fetched", "error", "blacklisted"...)**
- **error_name:** *string*
- **error_params:** *string*
- **searches:** *array*, the ids of the SEARCHES that match this url (mainly based on vocab and QUERIES)
- **real_url:** *string*, the url after following redirections
- **real_url_hash:** *string*
- **download_date:** *date*

### - real_urls

- **touched:** *boolean*, roughly states if the real urls is in the queue for quasi-equal-deduper.py (unless it is already in a cluster)

(mostly used by query-urls.py:)

- **url:** *string*, url of the downloaded page
- **hash:** *string*
- **content_html:** *string*
- **last_download_date:** *date*
- **clean_texts:** *array*, all the different texts of the tweets linking to this url
- **tweets_id:** *array*, the tweets linking to this url
- **sources:** *array*, the union of sources of all tweets linking to this url
- **searches:** *array*, the ids of the SEARCHES that match this url
- **urls:** *array*, all the urls linking to this url after following redirections
- **creation_date:** *date*
- **error:** *string*, ex: the HTML file is too big to fit in mongodb

(mostly used by html-content-extractor.py:)

- **html_extr_version:** *int*, the version of the html extractor at the time when the content was processed
- **ngrams_extr_version:** *int*, the version of the ngrams extractor at the time when the content was processed

(mostly set by html-content-extractor.py and read by quasi-equal-deduper.py:)

- **content_raw_text:** *string*, the text extracted by html2text without further normalization
- **content_text:** *string*, `content_raw_text` without the uninteresting lines
- **content_clean:** *string*, the result of applying text normalization to `content_text`

- **txt_len:** *int*, the length of `content_clean`
- **txt_lang:** *string*, the language determined based on content **(may be interesting to compare with the values by Twitter)**
- **txt_title:** *string*, extracted from the `<title>` tag

- **most_common_words, most_common_ngrams:** *array*, the most common words/ngrams found in the text and their count
- **idf_word, idf_ngram:** *array*, the words/ngrams with the highest tfidf scores found in the text

- **page_dict:** *array*, the beginning and length of each line (used for comparison)
- **good_for_clusters:** *boolean*, equals to True if the content is long enough to be considered by dedup

(mostly used by quasi-equal-deduper.py:)

- **in_cluster:** *string*, the id of the cluster (ie. content document) in which the url is.

(mostly used by get-stats.py:)

- **stats:** *dict*, some indicators of the url popularity


### - contents

- **touched:** *boolean*, roughly states if the content is in the queue for get-stats.py (unless it has already been sent in a mail)

(mostly used by quasi-equal-deduper.py:)

- **instances:** *array*, all the page_dicts of real_urls in the cluster
- **real_urls:** *array*, all the real_urls of tweets in the cluster
- **all_urls:** *array*, all the urls (ie. including shortened links) of tweets in the cluster
- **tweets_id:** *array*, the tweets in this cluster
- **txt_langs, txt_lens, txt_titles:** *arrays*, aggregation of the values from all the real_urls of the cluster
- **creation_date:** *date*, when the first link of the cluster appeared


- **last_update:** *date*, last activity about a tweet in the cluster
- **sources:** *array*, union of the QUERIES from all tweets in the cluster
- **searches:** *array*, union of the SEARCHES from all tweets in the cluster
- **sent:** *array*, list of the SEARCHES for which the content has been sent by mail
- **sfw:** *boolean*, equals to False if a word related to porn is detected in the title
- **stats:** *dict*, some indicators of the content popularity (sum of popularity of all tweets linking to this content). Includes:
	- **friends_count, followers_count, retweet_count, favorite_count, nb_tweets:** *int*, popularity indicators
	- **score:**, *int*, a weighted sum (weights in config file) of popularity indicators
	- **best_url:**, *string*, the most popular real url in the cluster  (used in the report)
	- **date:**, *date*, when the scores where computed for the last time


