# Introduction

Tobswen is a bot taking advantage of people curation work on social networks to display relevant and recent content about a given topic.

# Main steps

1. Get links from users posts on Twitter (collect.py)
2. Filter them and download webpages (query-urls.py)
3. Extract content (text and relevant words/phrases) from the html (html-content-extractor.py)
4. Group similar webpages into clusters of content (quasi-equal-deduper.py)
5. Score contents popularity and send daily reports

# Installation Notes

## package requirements

```bash
# from tobswen home folder
sudo pip3 install -r requirements.txt
```

## tobswen requirements

### MongoDB

You need to install mongodb, here is the installation notes for [ubuntu](https://docs.mongodb.com/v3.2/tutorial/install-mongodb-on-ubuntu/#import-the-public-key-used-by-the-package-management-system) 

# Important distinction

There is two non-obvious notions that should be distinguished (so they are written in CAPITALS in this doc): QUERIES and SEARCHES

- **QUERIES** (defined in the config file) represent a source of tweets (so they are sometimes abusively called "sources" in the code). They are defined by a set of keywords sent to Twitter API and the types of sources ("search" and/or "list") they support. They are identified by a unique id.

- **SEARCHES** (also defined in the config file) represent a specific interest, for which is generated a daily report. It's a way to filter the tweets fetched by selected queries over specific vocabulary. It doesn't affect which tweets are fetched. It only defines a subselection of tweets of interests whose linked content should be downloaded and reported. They are also identified by a unique id.

Example:
- QUERY: `{"id": "ces2017", "keywords": "ces2017 filter:links", sources: ["search", "list"]}` 
- SEARCHES: 
```javascript
{
	"id": "CES AI",
    	"queries": [{"id": "ces2017", "sources": ["search"]}],
		"vocab": ["ia", ["intelligence", "artificielle"]],
		"mails": ["address1@gmail.com", "address2@gmail.com"],
		"langs": ["fr"]
	},{"id": "CES cars",
    	"queries": [{"id": "ces2017", "sources": ["search"]}],
		"vocab": ["car", ["self", "driving"], ["conduite", "autonome"], "voiture", "truck"],
		"mails": ["address1@gmail.com", "address2@gmail.com"],
		"langs": ["en", "fr"]
	}
```
- All the tweets about CES are downloaded, but among them, only those about artificial intelligence in French or about cars will be selected to download linked content and be part of the two generated reports.
- Note that we could also have one SEARCH aggregating and filtering content from multiples QUERIES

# Schema and video

This schema along with the detailed explanation video (French) provides more understanding about the whole process (also read the [Collections](collections.md) section)

![](schema.jpg)

[full size](schema.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/QX8di1u8EIY" frameborder="0" allowfullscreen></iframe>


