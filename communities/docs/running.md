# Running

There are 5 scripts that need to be run concurrently to have a fully functionnal bot.
Those scripts are stored as notebooks on the git repo. Always make sure to convert them to scripts before starting in order to have the last changes available. The command is `jupyter nbconvert --to python the_notebook_name.ipynb`

It is advised to run each script in its own screen to be able to remotely manage them.

# Contributing

The project is still in an early stage of development, so we chose to keep all the code in notebooks instead of .py files to make further experiments easier. ALWAYS edit these notebook and then generate the .py files. NEVER directly update the .py files as it is not possible to automatically pass the changes on the notebooks. The .py files have been removed from the VCS on purpose to enforce this behavior.

Before commiting changes in notebooks, it's probably a good idea to clear output of all cells so the diff stays understandable.

# Testing

For debugging/improving/monitoring purposes, it's easier to play directly with notebooks. Be careful about side-effects: a lot of function calls have direct (and probably unreversible) impact on the database. Some functions that have an `apply` parameter that could be set to False so no writes are done to the database. It's probably a good idea to use a copy of the database to avoid losing data.

# Backups

To backup a database use `mongodump -d database_name -o output_folder` (here database_name should be `search_subject_v3` (this name should probably be improved btw))

To restore a backup use `mongorestore path/to/backup`

# Warning

- When adding new find queries to mongodb, be sure that it is indexed !
- The size of the database increase quickly, soon it will not entirely fit in memory/hard drive.