# Improvements

Some possible improvements of varying complexity and priority:

- `very easy` `needed` send an alert by mail when a process crashes (do not silently restart it, if a bug occurred it will probably keeps crashing in a loop). See https://docs.python.org/3/library/sys.html#sys.excepthook
- `easy` `needed` allow vocabulary exclusion in SEARCHES
- `easy` `needed` keep the hashtags attached to the content, in a way similar to the clean_texts attribute
- `easy` `needed` put a timeout (similar to query-urls.py) on html2text extraction because sometimes it can takes several minutes for one page (why? maybe because of encoding/special chars/bad html/page size?)
- `easy` `cleaning` remove lists because their results aren't as good as expected and they complicate a lot the code
- `medium` `nice-to-have` improve html2text to make it faster and more suited to our needs
- `medium` `nice-to-have` only keep the most pertinent/popular hashtags
- `medium` `nice-to-have` reload tweets before calculating popularity in order to have the latest indicators. Need to be done in a smart way because of the huge overload it could cause.
- `medium` `nice-to-have` improve the NSFW filter (maybe first step: ban authors of porn tweets)
- `medium` `bonus` crawl with JS enabled (Selenium?). Probably huge charge overload...
- `medium` `scalability` have a different scheduler for each QUERY based on the volume of tweets fetched to save requests to the API/don't miss tweets
- `medium` `next-step` take the content (extracted ngrams!) of the webpage into account to assess the pertinence of a content
- `hard` `next-step` group contents speaking about the same event with different words (ie. Google news "stories"). Maybe use ngrams/named entities but won't be easy...
- `hard` `scalability` `inevitable` deal with the mongodb ever-increasing size...
- `hard` `experiment` base queries on **communities** (from users graph) instead of search/list of users found in search. This is a huge work but will possibly have a dramatic impact on the quality of the reports
