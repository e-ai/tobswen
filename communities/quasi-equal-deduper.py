
# coding: utf-8

# In[ ]:

import re
from pymongo import MongoClient
import time
import pandas as pd
from tqdm import tqdm
import numpy as np
from frozendict import frozendict
from os import listdir, makedirs
from os.path import isfile, join
from utils import only_id, freeze, unfreeze
from datetime import datetime, timedelta
from uuid import uuid4
import json
import os


# In[ ]:

# mongodb collections
client = MongoClient()
db = client.search_subject_v3
real_urls_collection = db.real_urls
contents_collection = db.contents


# In[ ]:

# mongodb indexes
contents_collection.create_index([("last_udpate", -1)])
contents_collection.create_index([("searches", 1)])
contents_collection.create_index([("sent", 1)])


# In[ ]:

# params for scoring similarity
max_match_len = 5000  # matching more than this length won't improve score further
score_matched_factor = 2  # give more importance to the length of matched content vs inclusion
min_score = 1.8  # it's considered a match above this threshold


# In[ ]:

# computes the similarity between two page_dicts
def pd_dist(u1, u2):
    inter = frozenset(set(u1["page_dict"]) & set(u2["page_dict"]))
    nb_matches = len(inter)
    len_matched = sum([min(u1["page_dict"][x], u2["page_dict"][x]) for x in inter])
    
    return nb_matches, len_matched, inter


# In[ ]:

# weight the previous function output to give a score
def score_pair(u1, u2):
    nb_matches, len_matched, _ = pd_dist(u1, u2)
            
    score_matched_len = min(1, len_matched / max_match_len)
    score_1_in_2 = len_matched / (u1["txt_len"] + 1)
    score_2_in_1 = len_matched / (u2["txt_len"] + 1)
    return score_matched_factor * score_matched_len + score_1_in_2 + score_2_in_1, score_matched_len, score_1_in_2, score_2_in_1


# In[ ]:

# list of attributes of contents which are sets so we can apply specific processing (freezing & unfreezing)
# because python refuses to store list/dict/set in a set (you need to freeze it)
# and mongodb can only dedup with $addToSet if it's int/string values, not dict/list...
set_keys = ["instances", "real_urls", "all_urls", "tweets_id", "sources", "searches", "txt_langs", "txt_lens", "txt_titles", "sent"]


# In[ ]:

# compare a real_url content with existing clusters and add to/create/merge clusters depending on matches
def process_one(url, apply=False):
    found_in = set()
    for c in contents.values():
        nb_links = 0
        for dst in c["instances"]:
            if score_pair(url, dst)[0] > min_score:
                nb_links += 1
        # we decided that a real_url must match with at least 20% of the members of the cluster to be added to it
        if nb_links > len(c["instances"]) / 5:
            found_in.add(c["_id"])
            
    new_cluster = {"touched": True, "last_update": datetime.now()}
    
    content_from_url = {
        "instances": {freeze({"txt_len": url["txt_len"], "page_dict": url["page_dict"]})}, 
        "real_urls": {url["url"]},
        "all_urls": set(url["urls"]),
        "tweets_id": set(url["tweets_id"]),
        "sources": {freeze(x) for x in url["sources"]},
        "searches": set(url["searches"]),
        "txt_langs": {url["txt_lang"]},
        "txt_lens": {url["txt_len"]},
        "txt_titles": {url["txt_title"]},
        "sent": set(),
    }
    
    for k in set_keys:
        # the new cluster is the union of all clusters matched with the url and the values of the url itself
        new_cluster[k] = set.union(content_from_url[k], *[contents[i][k] for i in found_in])
        # every values of content[i] is already frozen (when loading from mongodb) so we can use set operations
    
    new_cluster["creation_date"] = min([datetime.now()] + [contents[i]["creation_date"] for i in found_in])
    
    if verbose:  # print all the steps for debugging purposes
        if len(found_in) == 0:
            print("Add one cluster:", url["url"], "\n")
        else:
            print("Merge clusters, removed ", len(found_in))
            for x in new_cluster["real_urls"]:
                print(x)
            print()
    
    # replace all the clusters with a new one
    for i in found_in:
        if apply:
            contents_collection.delete_one({"_id": i})
        del contents[i]
    if apply:
        new_id = contents_collection.insert_one({k: [unfreeze(x) for x in new_cluster[k]] if k in set_keys else new_cluster[k] for k in new_cluster}).inserted_id
        for u in new_cluster["real_urls"]:
            real_urls_collection.update_one({"url": u}, {"$set": {"in_cluster": new_id}})
    else:
        new_id = uuid4().hex
    new_cluster["_id"] = new_id
    contents[new_id] = new_cluster
    
    if apply:
        real_urls_collection.update_one(only_id(url), {"$set": {"touched": False}})


# In[ ]:

# wrapper to run a function on a collection with a given mongo query
# apply: set to False for debugging when you need to compute/see clusters without impacting mongodb
def run_all(coll, fun, pool, continuous=False, apply=True):
    done = False
    while not done or continuous:
        nb_urls = coll.count(pool)
        if os.path.isfile("blocked.txt"):
            print("wait for the report generation completion")
            time.sleep(10)
        elif nb_urls == 0:
            print("nothing to do...")
            time.sleep(10)
        else:
            load_contents()
            try:
                with open("config.json") as f:
                    history_size = timedelta(days=json.load(f)["clusters_persistence_in_days"])
            except Exception as e:
                print("WARNING: INCORRECT JSON CONFIG! USING THE ONE IN MEMORY. FIX FAST.\nException:", e)
            for entry in tqdm(coll.find(pool), total=nb_urls):    
                fun(entry, apply=apply)
        done = True


# In[ ]:

# we fetch the clusters from mongo only once every run, then the modification are done in-memory
def load_contents():
    global contents
    mongo_query = {"last_update": {"$gt": datetime.now() - history_size}}  # only recent clusters are considered
    contents = {c["_id"]: c for c in contents_collection.find(mongo_query)}
    for c in contents.values():
        for k in set_keys:
            if k in c:
                c[k] = {freeze(x) for x in c[k]}
            else:
                c[k] = {}


# In[ ]:

# used for debug
verbose = False


# In[ ]:

# used to get the param when working in the notebook without calling run_all
with open("config.json") as f:
    history_size = timedelta(days=json.load(f)["clusters_persistence_in_days"])


# In[ ]:

# some commented code used for debug
#run_all(real_urls_collection, process_one, {"page_dict": {"$exists": True}}, apply=False)


# In[ ]:

#real_urls_collection.count({"in_cluster": {"$exists": False}, "good_for_clusters": True, })


# In[ ]:

#real_urls_collection.update_many({}, {"$unset": {"in_cluster": ""}, "$set": {"touched": True}})


# In[ ]:

# the actual function call. Process all real_url not in a cluster, successfully extracted and touched
run_all(real_urls_collection, process_one, {"in_cluster": {"$exists": False}, 
                                            "good_for_clusters": True, "touched": True}, continuous=True, apply=True)


# In[ ]:

# some printing below, used for debug inside the notebook
print("Nb clusters:", contents_collection.count())
print("Dont paires:", contents_collection.count({"real_urls": {"$size": 2}}))
print("Dont triplets:", contents_collection.count({"real_urls": {"$size": 3}}))
print("Dont >3 élements:", contents_collection.count({"real_urls.3":  {"$exists": True}}))

