import re
from frozendict import frozendict
import pandas as pd
from collections import Counter
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import json

def random_doc(coll, query={}, num=1):
    if num == 1:
        return coll.aggregate([{"$match": query}, {"$sample": {"size": 1}}]).next()
    else:
        return list(coll.aggregate([{"$match": query}, {"$sample": {"size": num}}]))

def only_id(d):
	return {"_id": d["_id"]}

def freeze(a):
    if type(a) == list:
        return frozenset({freeze(x) for x in a})
    if type(a) == dict:
        return frozendict({k: freeze(a[k]) for k in a})
    return a

def unfreeze(a):
    if type(a) == frozenset:
        return [unfreeze(x) for x in a]
    if type(a) == frozendict:
        return {k: unfreeze(a[k]) for k in a}
    return a

def csv_to_list_dict(path):
    df = pd.read_csv(path)
    return Counter({x["word"]: x["idf"] for x in df.to_dict(orient="records")})

def send_email(recipients, title, body):
    with open("keys.json") as f:
        gmail_creds = json.load(f)["gmail"]
    sender = gmail_creds["login"]
    password = gmail_creds["password"]

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    msg['Subject'] = title

    msg.attach(MIMEText(body, 'plain'))

    try:
        with smtplib.SMTP('smtp.gmail.com', 587) as server:
            server.ehlo()
            server.starttls()
            server.ehlo()
            server.login(sender, password)
            server.sendmail(sender, recipients, msg.as_string())
            server.close()
            
        print("{}: email sent!".format(title))
    except Exception as e:
        print("{}: unable to send email".format(title))
        raise e