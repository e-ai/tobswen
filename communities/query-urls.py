
# coding: utf-8

# In[20]:

from pymongo import MongoClient
from pymongo.errors import CursorNotFound
import requests
from tqdm import tqdm
from datetime import datetime, timedelta
from tldextract import extract as tldextract
from frozendict import frozendict
import time
import xxhash
import json
from utils import only_id


# In[21]:

# allows us to stop a function execution after a given time
import signal
from time import sleep

class TimeoutException(Exception): pass

def _timeout(signum, frame):
    raise TimeoutException()


# In[22]:

root_crawl_folder = 'datas/crawled/'  # where the fetched html files are located 
should_retry_errors = False  # if we fail to fetch an url and it appears again later, should we retry to download it? 
verbose = False  # output download logs in console or not


# In[23]:

# mongodb collections
client = MongoClient()
db = client.search_subject_v3
urls_collection = db.urls
real_urls_collection = db.real_urls
contents_collection = db.contents


# In[24]:

searches = None
config = None
# some urls return "403 forbidden" if no User-Agent is specified
user_agent = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'}
reload_searches_rate = 1000  # how much urls do we fetch before reloading the config file?

# TODO: crawl with Selenium to run JS?
# example of page not working: http://upflow.co/l/N6Wd


# In[25]:

# list of popular sites with no content to extract
with open("blacklist.json") as f:
    blacklist = json.load(f)


# In[26]:

# a custom generic exception raised if any errors appears during download
class DownloadException(Exception): pass


# In[27]:

# returns the content (and url after redirections) of a given url
def get_html(url, timeout_seconds=20):
    signal.signal(signal.SIGALRM, _timeout)
    signal.alarm(timeout_seconds)
    
    try:  # sometimes the async SIGALRM signal is called outside of the inner try/except
        try:  # force timeout of the request
            resp = requests.get(url, allow_redirects=True, headers=user_agent)  
        except TimeoutException:
            if verbose:
                print('Request timed out:', url)
            raise DownloadException("timeout")
        except Exception as e:  # an exception from requests lib
            if verbose:
                print("'{}' exception retrieving URL '{}': {}".format(type(e), url, e))
            raise DownloadException("unknown exception", {"exception": e})
        else:
            if not resp.ok:  # content fetched without any exception but error code
                if verbose:
                    print("Bad status code for URL '{}': {}".format(url, resp.status_code))
                raise DownloadException("bad status code", {"status_code": resp.status_code})
            elif not resp.headers.get('content-type', '').startswith('text/html'):  # content is not html
                if verbose:
                    print("Not a html url:", url)
                raise DownloadException("bad content type", {"content_type": resp.headers.get('content-type', None)})
            else: # valid response, & html file
                if "dnserrorassist" in resp.text:  # AT&T display custom page when the url doesn't exist
                    if verbose:
                        print("Got ATT DNS... Probably invalid URL:", url)
                    raise DownloadException("att dns error")
                else:
                    if verbose:
                        print("Successfully fetched:", url)
                    try:
                        return resp.url, resp.content.decode("utf-8")
                    except UnicodeDecodeError:  
                        # sometimes decoding with utf-8 does not work, use requests built-in instead
                        if verbose:
                            print("Error (ignored) converting to unicode:", url)
                        return resp.url, resp.text
    except TimeoutException:
        if verbose:
            print("Timeout after request ended: ignore it:", url)
    finally:
        signal.alarm(0)


# In[28]:

# call the html downloader function and saves the result in mongodb
def get_and_save_html(url, timeout_seconds=20):
    url_string = url["url"]
    if verbose:
        print("Start fetching:", url_string)
    try:
        real_url, html = get_html(url_string, timeout_seconds)
    except DownloadException as e:
        try:
            state = {"state": "error", "error_name": str(e.args[0])}
            if len(e.args) == 2:
                state["error_params"] = str(e.args[1])
        except:
            state = {"state": "error", "error_name": "error getting error name"}
                
        urls_collection.update_one(only_id(url), {"$set": state})
    else:
        update_url(url, real_url, html=html)
        update_real_url(url, html=html)


# In[29]:

# determines if an url has been found in the queries allowed by a given search
def find_search(url, search):
    u_queries = url["sources"]
    s_queries = search["queries"]
    for sq in s_queries:
        for uq in u_queries:
            if sq["id"] == uq["search_query"]:
                s_sources = sq.get("sources", [])
                if len(s_sources) == 0 or uq["source"] in s_sources:
                    return True
    return False


# In[30]:

# determines if an url (ie. the content of the tweets linking to it) contains keywords of a given search
def find_vocab(url, search):
    text = ' '.join(url["clean_texts"])
    vocab = search["vocab"]
    
    if len(vocab) == 0:
        return True
    
    for p in vocab:
        if type(p) == str:
            if p in text:
                return True
        else:
            if all(x in text for x in p):
                return True
    
    return False


# In[31]:

# set the searches field of an url (ie. the searches which it complies to)
# it will determine if the content of the page should be fetched or not
# return True if new searches are found
def update_searches(url):
    previous_searches = url.get("searches", {})
    new_searches = []
    for search in searches:
        if find_search(url, search) and find_vocab(url, search):
            new_searches.append(search["id"])
    
    url["searches"] = new_searches
    
    urls_collection.update_one(only_id(url), {"$addToSet": {"searches": {"$each": url["searches"]}}})
    return len(set(new_searches) - set(previous_searches)) > 0


# In[32]:

# when an url if successfully fetched (or linked to another url fetched before)
def update_url(url, real_url, html=None, state="fetched"):
    url_hash = xxhash.xxh64(real_url).hexdigest()
    set_dict = {
        "state": state, 
        "real_url": real_url,  # url after redirections
        "real_url_hash": url_hash,
    }
    
    if html is not None:
        set_dict["download_date"] = datetime.now()
        with open(root_crawl_folder + url_hash + '.html', 'w') as f:
            f.write(html)
    urls_collection.update_one(only_id(url), {"$set": set_dict})
    for k in set_dict:
        url[k] = set_dict[k]


# In[33]:

# whenever an update (new tweet, new content, new source...) occurs to an url it is passed on its real_url
def update_real_url(url, html=None):
    set_dict = {"hash": url["real_url_hash"], "touched": True}
    if html is not None:
        set_dict["content_html"] = html
        set_dict["last_download_date"] = datetime.now()
    try:
        real_urls_collection.update_one({"url": url["real_url"]}, {"$set": set_dict, "$addToSet": {
                    "clean_texts": {"$each": url["clean_texts"]}, 
                    "tweets_id": {"$each": url["tweets_id"]}, 
                    "sources": {"$each": url["sources"]},
                    "searches": {"$each": url["searches"]},
                    "urls": url["url"],
                }, "$setOnInsert": {"creation_date": datetime.now()}}, upsert=True)
    except Exception as e:  # can happen for example if the html content size is too large
        real_urls_collection.update_one({"url": url["real_url"]}, {
                "$set": {"error": "could not insert in db"},
                "$setOnInsert": {"creation_date": datetime.now()}
            }, upsert=True)
        print("ERROR DURING INSERTION IN DB:", e)
        
    real_url = real_urls_collection.find_one({"url": url["real_url"]})
    cluster_id = real_url.get("in_cluster")
    if cluster_id is not None:  # also pass the update on the content collection
        contents_collection.update_one({"_id": cluster_id}, {"$set": {"touched": True}})


# In[34]:

# complete processing of an url in the queue
def process_one_url(url):
    update_searches(url)
    
    url_string = url["url"]
    url_state = url.get("state", None)
    
    if url_state == "fetched":
        update_real_url(url)
    elif url_state == "error":
        if len(url.get("searches", [])) > 0 and should_retry_errors:
            get_and_save_html(url)
    elif url_state == "blacklisted":
        pass
    else:  # State does not exists ()
        same_one = real_urls_collection.find_one({"url": url_string})

        if same_one is not None:
            if verbose:
                print("URL already fetched:", url_string)
            update_url(url, url_string, state="already fetched")
            update_real_url(url)
        elif tldextract(url_string).domain in blacklist:
            if verbose:
                print("URL blacklisted:", url_string)
            urls_collection.update_one(only_id(some_url), {"$set": {"state": "blacklisted"}})
        elif len(url.get("searches", [])) > 0:
            get_and_save_html(url)
        else:
            urls_collection.update_one(only_id(some_url), {"$set": {"state": "ignored for now"}})
            if verbose:
                print("URL ignored:", url_string)
    
    urls_collection.update_one(only_id(url), {"$set": {"touched": False}})


# In[35]:

# look for changes in the config file on a regular basis
# makes sure not to crash if there is an error in the config.json (ex: when the file is being edited) 
# by saving and using the previous config file
def reload_searches():
    global searches, config
    if searches is not None:
        old_searches = searches
        with open("old_searches.dat", "w") as f:
            json.dump(searches, f)
    else:
        try:
            with open("old_searches.dat") as f:
                old_searches = json.load(f)
        except FileNotFoundError:
            old_searches = None
    
    try:
        with open("config.json") as f:
            config = json.load(f)
            searches = config["searches"]
    except Exception as e:
        print("WARNING: INCORRECT JSON CONFIG! USING THE ONE IN MEMORY. FIX FAST.\nException:", e)
    
    if searches != old_searches:
        rerun_searches()


# #### This function must be run when the config.json file is modified in order to find urls ignored in previous runs but interesting for the new searches:

# In[36]:

def rerun_searches():
    hours = config["rerun_searches_hours_history"]
    print("Config file changed, re-running searches on the last {} hours".format(hours))
    for url in tqdm(urls_collection.find({"creation_date": {"$gt": datetime.now() - timedelta(hours=hours)}})):
        if update_searches(url):
            urls_collection.update_one(only_id(url), {"$set": {"touched": True}})


# In[37]:

reload_searches()


# In[38]:

# the running loop
while True:
    nb_urls = urls_collection.count({"touched": True})  # only consider url updated since last run
    if nb_urls == 0:
        print("nothing to do...")
        reload_searches()
        time.sleep(5)
    else:
        url_queue = urls_collection.find({"touched": True})
        try:
            for i, some_url in tqdm(enumerate(url_queue), total=nb_urls):
                if i % reload_searches_rate == 0:  # in case of loooong queue, don't wait for the next run to reload
                    reload_searches()
                process_one_url(some_url)
        # an exception raised if no call is made to mongodb during several secs (can happen when the DL is long)
        except CursorNotFound:
            pass


# In[ ]:



