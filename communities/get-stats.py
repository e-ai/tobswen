
# coding: utf-8

# In[ ]:

from pymongo import MongoClient
import re
import time
import json
import pandas as pd
from tqdm import tqdm
import numpy as np
from frozendict import frozendict
from os import listdir, makedirs
from os.path import isfile, join
from utils import only_id, freeze, unfreeze, send_email
from datetime import datetime, timedelta
from collections import Counter
from operator import itemgetter
import schedule
from nltk.tokenize import wordpunct_tokenize
import os


# In[ ]:

# mongodb collections
client = MongoClient()
db = client.search_subject_v3
status_collection = db.statuses
real_urls_collection = db.real_urls
contents_collection = db.contents
nsfw_filtered_tweets = ""


# In[ ]:

# score a content popularity using weights adjustable in config file
def score(stat):
    return sum(score_params[k]["factor"] * min(1, stat[k] / score_params[k]["max"]) for k in score_params if k in stat)


# In[ ]:

# aggregate popularity indicators on multiple levels (tweets -> real_urls -> content) 
# to get the total content popularity
# set cache to True if you want to use the last computed scores (way faster for debugging purposes)
def get_stats_one(content, cache=False):
    all_acc = {}
    agg_stats = Counter()
    users = []

    if cache and "stats" in content:
        return content["stats"]
    
    for u in content["real_urls"]:
        url = real_urls_collection.find_one({"url": u})
        tweets_id = url["tweets_id"]
        ru_acc = Counter({"nb_tweets": len(tweets_id)})  # "real_url_accumulator"
        for tid in tweets_id:
            status = status_collection.find_one({"tweet.id": tid})
            user = status["tweet"]["user"]
            
            # TODECIDE: Filter retweets?
            if status["is_retweet"]: pass#continue
            
            tweet_scores = {
                "friends_count": user["friends_count"], 
                "followers_count": user["followers_count"], 
                "retweet_count": status["tweet"]["retweet_count"],
                "favorite_count": status["tweet"]["favorite_count"]
            }
            
            # Add scores to real url
            ru_acc.update(tweet_scores)
            
            tweet_scores["date"] = datetime.now()
            tweet_scores["score"] = score(tweet_scores)
            # optional: update status stats (x1.5 slower...)
            #status_collection.update_one(only_id(status), {"$set": {"stats": tweet_scores}})
            
            # TODECIDE: Filter user dups?
            users.append(user["screen_name"])
        
        ru_acc["score"] = score(ru_acc)
        agg_stats.update(ru_acc)
        
        ru_acc["url"] = u
        all_acc[u] = ru_acc
        
        ru_acc["date"] = datetime.now()
        real_urls_collection.update_one(only_id(url), {"$set": {"stats": ru_acc}})
    
    agg_stats = dict(agg_stats)
    agg_stats["score"] = score(agg_stats)
    agg_stats["date"] = datetime.now()
    
    # Choose the real_url with the highest score as a representative of the content
    best_ru = max(all_acc.values(), key=itemgetter("score"))

    agg_stats["best_url"] = best_ru["url"]
    
    content["stats"] = agg_stats
    contents_collection.update_one(only_id(content), {"$set": {"stats": content["stats"]}})
    
    return agg_stats


# In[ ]:

# check for porn words presence
def sfw(s):
    words = wordpunct_tokenize(s.lower())
    return not any(w in words for w in nsfw_words)


# In[ ]:

# returns a string describing a content (then added to the mail body), and if it is sfw
def entry_to_body(entry):
    c = contents_collection.find_one({"_id": entry["content_id"]})
    u = real_urls_collection.find_one({"url": entry["best_url"]})
    
    u_title = re.sub(r"\s+", " ", u["txt_title"] if u["txt_title"] is not None else "Unknown title")
    
    s = "\n{}\n({} tweets, score: {:2.2f}, lang: {}, length: {})\n{}"
    s = s.format(u_title, c["stats"]["nb_tweets"], entry["score"], u["txt_lang"], u["txt_len"], entry["best_url"])
    if len(c.get("sent", [])) > 0:
        s += '\n(also seen in "{}")'.format('", "'.join(c["sent"]))
    
    return s, sfw(u_title)


# In[ ]:

# compute scores, make report and optionally send mail to users attached to the search
# set verbose to True to print the report
# set apply to False when debugging 
def process_search(search, apply=False, should_send_email=False, cache=True, verbose=True):
    global nsfw_filtered_tweets
    
    if cache:
        print("Warning: stats caching activated")
        
    mongo_query = {"last_update": {"$gt": datetime.now() - history_size}, 
                   "searches": search["id"], 
                   "sent": {"$not": {"$eq": search["id"]}},
                   "txt_langs": {"$in": search["langs"]}, 
                   "touched": True}
    
    contents = contents_collection.find(mongo_query)
    
    hof = []
    for c in tqdm(contents):
        stats = get_stats_one(c, cache=cache)
        hof.append({"content_id": c["_id"], "best_url": stats["best_url"], "score": stats["score"]})
    top_n = search.get("top_n", config["default_top_n"])
    hof = sorted(hof, key=itemgetter("score"), reverse=True)[:top_n]
    
    title = "{} summary on '{}' (top {})".format(datetime.now().strftime("%b %d %Y"), search.get("description", search["id"]), top_n)
    
    entries_as_string = [entry_to_body(x) for x in hof]
    
    full_body = title + '\n(' + str(len(hof)) + " items)" + '\n\n\n' + '\n'.join(s for s, safe in entries_as_string if safe)
    
    nsfw_filtered_tweets += '\n'.join(s for s, safe in entries_as_string if not safe)

    if should_send_email:
        send_email(search["mails"], title, full_body)
    if verbose:
        print(full_body)
        print('____________________\n\n\n')
    
    if apply:
        for entry, (s, safe) in zip(hof, entries_as_string):
            contents_collection.update_one({"_id": entry["content_id"]}, 
                                           {"$set": {"sfw": safe}, "$addToSet": {"sent": search["id"]}})


# In[ ]:

with open("config.json") as f:
    config = json.load(f)
    searches = config["searches"]
    score_params = config["score_params"]
    history_size = timedelta(days=config["clusters_persistence_in_days"])
    nsfw_words = config["nsfw_words"]


# In[ ]:

# run all the searches report (plus the NSFW summary for debug purposes)
def send_all(apply=False, cache=True, should_send_email=False, verbose=False):
    global searches, config, nsfw_filtered_tweets
    with open("blocked.txt", "w") as f:
        f.write(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    time.sleep(10)
    
    try:
        with open("config.json") as f:
            config = json.load(f)
            searches = config["searches"]
            score_params = config["score_params"]
            history_size = timedelta(days=config["clusters_persistence_in_days"])
    except Exception as e:
        print("WARNING: INCORRECT JSON CONFIG! USING THE ONE IN MEMORY. FIX FAST.\nException:", e)
    for s in searches:
        process_search(s, apply=apply, cache=cache, should_send_email=should_send_email, verbose=verbose)
    
    if apply:
        contents_collection.update_many({'touched': True}, {"$set": {"touched": False}})
        
    if should_send_email:
        send_email(config["nsfw_mails"], "NSFW Tweets of the day", nsfw_filtered_tweets)
    else:
        print("NSFW Tweets:\n\n" + nsfw_filtered_tweets)
    nsfw_filtered_tweets = ""
    
    try:
        os.remove("blocked.txt")
    except FileNotFoundError:
        pass


# In[ ]:

try:
    os.remove("blocked.txt")
except FileNotFoundError:
    pass


# In[ ]:

#contents_collection.update_many({}, {"$unset": {"sent": 1}, "$set": {"touched": True}})


# ## Note: to see the current state without affecting the daily output nor sending an email use:

# In[ ]:

#send_all(apply=False, cache=True, should_send_email=False, verbose=True)


# ## Scheduled run:

# In[ ]:

schedule.every().day.at('19:30').do(lambda: send_all(apply=True, cache=False, should_send_email=True))


# In[ ]:

while True:
    schedule.run_pending()
    time.sleep(100)

