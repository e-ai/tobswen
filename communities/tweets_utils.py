import tweepy
import json
from nlumtp.preprocessors.cleaners.whitelist import CleanChars
import re
from nltk.corpus import stopwords

def to_json(results):
    return [r._json for r in results]

def filter_vocab_one(tweet, vocab, invert=False):
    if type(tweet) == str:
        if any([v in t.lower() for v in vocab]) != invert:
            return True
    elif type(tweet) == dict:
        if any([v in tweet["text"].lower() for v in vocab]) != invert:
            return True
    else:
        if any([v in tweet.text.lower() for v in vocab]) != invert:
            return True
    return False

def filter_vocab(tweets, vocab, invert=False):
    r = [t for t in tweets if filter_vocab_one(t, vocab, invert)]
    print(len(tweets) - len(r), '/', len(tweets), "tweets filtered by vocab")
    return r

def filter_retweets_one(tweet):
    if type(tweet) == str:
        if not tweet.startswith("RT"):
            return False
    elif type(tweet) == dict:
        if not tweet["text"].startswith("RT"):
            return False
    else:
        if not tweet.text.startswith("RT"):
            return False
    return True

def filter_retweets(tweets):
    r = [t for t in tweets if filter_retweets_one(t)]
    print(len(tweets) - len(r), '/', len(tweets), "tweets filtered by RT")
    return r

def filter_urls_http(tweets):
    if len(tweets) == 0:
        return []
    if type(tweets[0]) == str:
        return [t for t in tweets if 'http' in t]
    elif type(tweets[0]) == dict:
        return [t for t in tweets if not 'http' in t["text"]]
    else:
        return [t for t in tweets if not 'http' in t.text]

def filter_urls(tweets):
    if len(tweets) == 0:
        return [], []
    elif type(tweets[0]) == dict:
        tweets_with_url = [t for t in tweets if t["entities"].get("urls", []) != []]
        print(len(tweets) - len(tweets_with_url), '/', len(tweets), "tweets filtered by url")
        return tweets_with_url, [t["entities"]["urls"][0]["expanded_url"] for t in tweets_with_url]
    else:
        tweets_with_url = [t for t in tweets if t.entities.get("urls", []) != []]
        print(len(tweets) - len(tweets_with_url), '/', len(tweets), "tweets filtered by url")
        return tweets_with_url, [t.entities["urls"][0]["expanded_url"] for t in tweets_with_url]

def remove_entities_one(t, interest=["symbols", "urls", "user_mentions"]):
    ranges = []
    for k in t["entities"]:
        if k in interest:
            ranges.extend([r["indices"] for r in t["entities"][k]])

    result = t["text"]
    for k in list(reversed(sorted(ranges))):
        result = result[:k[0]] + result[k[1]:]
    
    return result

def filter_entities_one(t, interest=["symbols", "urls", "user_mentions"]):
    t["text"] = remove_entities_one(t, interest)
    return t

def filter_entities(tweets, interest=["symbols", "urls", "user_mentions"]):
    return [filter_entities_one(t, interest) for t in tweets]

# unused
def get_language(input_text):
    input_words = input_text.lower().split()
    likelihoods = {l: len(set(input_words) & set(stopwords.words(l))) for l in stopwords._fileids}
    return max(likelihoods, key=likelihoods.get)

def filter_language(tweets, languages=["english", "french"], langs=["en", "fr", "und"]):
    filtered = [t for t in tweets if t["lang"] in langs]
    #filtered = [t for t in tweets if get_language(t["text"]) in languages or t["lang"] in langs]
    trash = [t for t in tweets if t["lang"] not in langs]
    print(len(trash), '/', len(tweets), "tweets filtered by language")
    return filtered, trash
    
def ttt(tweets):
    if len(tweets) == 0:
        return []
    if type(tweets[0]) == dict:
        return [t["text"] for t in tweets]
    else:
        return [t.text for t in tweets]
    
cc = CleanChars([
        [r"(?:https?|www|[\w.]+\.\w+\/)\S*", ""],
        [r"[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+", ""],
        [r"(?:[" + re.escape(CleanChars.REPLACE_WITH_SPACE_FR) + "\n" + "]+|  +)", " "],
        [r"[^" + CleanChars.ALLOWED_CHARS_FR + "0123456789" + "]", ""],
    ])
all_stopwords = set(stopwords.words("english") + stopwords.words("french"))

def clean_chars_one(text):
    return ' '.join([w for w in cc.preprocess_string_dataset(text).split() if w not in all_stopwords])

def clean_chars(tweets):
    for t in tweets:
        t["text"] = clean_chars_one(t["text"])
    return tweets

def get_twitter_api(keys="keys.json"):
    if type(keys) == str:
        with open(keys) as f:
            keys_dict = json.load(f)["twitter"]
    elif type(keys) == dict:
        keys_dict = keys.copy()

    auth = tweepy.OAuthHandler(keys_dict["consumer_key"], keys_dict["consumer_secret"])
    auth.set_access_token(keys_dict["access_key"], keys_dict["access_secret"])
    return tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
