
# coding: utf-8

# In[ ]:

import tweepy
from tqdm import tqdm
import json
from datetime import datetime, timedelta
from pymongo import MongoClient
from tweets_utils import get_twitter_api, remove_entities_one, clean_chars_one
import time
import schedule
from nlumtp.preprocessors.cleaners.whitelist import CleanChars


# ## Common

# In[ ]:

# mongodb collections
client = MongoClient()
db = client.search_subject_v3
status_collection = db.statuses
posters_collection = db.posters
urls_collection = db.urls
real_urls_collection = db.real_urls


# In[ ]:

# mongodb indexes
urls_collection.create_index([("url", "hashed")])
urls_collection.create_index([("creation_date", 1)])
status_collection.create_index([("tweet.id", "hashed")])
status_collection.create_index([("tweet_download_date", 1)])
posters_collection.create_index([("twitter_id", "hashed")])
real_urls_collection.create_index([("url", "hashed")])


# In[ ]:

# load config at start
with open("config.json") as f:
    config = json.load(f)


# In[ ]:

class OurException(Exception): pass # just a generic, custom exception


# In[ ]:

# look for the last downloaded tweet for a query in the db in order to resume tweets fetching 
# and avoid downloading them multiple times
def get_last_tweet_id(source, query_id):
    if source == "search":
        mongo_query = {"sources": {"$elemMatch": {"source": "search", "search_query": query_id}}}
    elif source == "list":
        mongo_query = {"sources": {"$elemMatch": {"list.name": list_name_from_query(query_id), 
                                                  "list.owner": config["owner_screen_name"]}}}
        
    last_tweet = status_collection.find_one({"$query": mongo_query, "$orderby": {"tweet.id": -1}})
    
    if last_tweet is not None:
        return last_tweet["tweet"]["id"]
    else:
        return 1


# In[ ]:

cc = CleanChars.only_alphanum_cleaner(line_break=False)


# In[ ]:

# upsert url collection when a new tweet contains a link
def update_url(tweet_id, url, clean_text, source_dict):
    urls_collection.update_one({"url": url}, {
            "$set": {"touched": True},
            "$addToSet": {
                "clean_texts": clean_text, 
                "tweets_id": tweet_id, 
                "sources": source_dict
            }, "$setOnInsert": {"creation_date": datetime.now()}}, upsert=True)


# In[ ]:

# process the json of one fetched tweet
def handle_status(status, source, query_id, the_list=None):
    status_json = status._json.copy()
    status_json["created_at"] = status.created_at  # To keep datetime object instead of string 
    status_json["user"]["created_at"] = status.user.created_at
    
    es = {
        "tweet": status_json,
        "tweet_download_date": datetime.now(),
        "clean_text": cc.preprocess_string_dataset(remove_entities_one(status_json)), 
        "tweet_language": status_json["lang"],
        "is_retweet": status_json["text"].startswith("RT")
    }
    
    source_dict = {"search_query": query_id, "source": source}
    if source == "list":
        source_dict["list"] = { "name": the_list.name, "owner": the_list.user.screen_name }
    
    for url_dict in status_json["entities"].get("urls", []):
        url = url_dict["expanded_url"]
        update_url(es["tweet"]["id"], url, es["clean_text"], source_dict)
        
    status_collection.update_one({"tweet.id": es["tweet"]["id"]}, {
            "$set": es, 
            "$push": {"sources": source_dict}
        }, upsert=True)
    
    user_json = status_json["user"]
    posters_collection.update_one({'twitter_id': user_json["id"]}, {
            "$set": {"twitter_user": user_json, "last_download_date": datetime.now()},
            "$addToSet": {"search_queries": query_id},
            "$inc": {"nb_tweets_fetched": 1},
            "$max": {"original_poster": not es["is_retweet"]}
        }, upsert=True)


# In[ ]:

# fetch all new tweets about a query using tweepy
def get_new_tweets(api, source, search_query, query_id, the_list=None):
    last_tweet_id = get_last_tweet_id(source, query_id)
    
    max_items=2000
    max_retry=5
    retry = 0
    done = False
    while retry < max_retry and not done:
        try:
            if source == "search":
                cursor = tweepy.Cursor(api.search, q=search_query, result_type="recent", count=1000, since_id=last_tweet_id).items(max_items)
            elif source == "list":
                cursor = tweepy.Cursor(api.list_timeline, list_id=the_list.id, count=1000, since_id=last_tweet_id).items(max_items)
            
            for status in tqdm(cursor, desc="{} - Collecting for '{}' ({})".format(datetime.now(), query_id, source)):
                handle_status(status, source, query_id, the_list)

        except Exception as e:
            retry += 1
            print("Cursor failed ({}/{} tries) in {} source for '{}': {}".format(retry, max_retry, source, query_id, e))
            time.sleep(5)
        else:
            done = True

    if not done:
        raise OurException


# ## List

# In[ ]:

# the name that will be used on Twitter
# TODO: make sure there is no collision in names and that the name isn't too long for Twitter API
def list_name_from_query(query_id):
    return query_id.replace(" ", '_') + '_the_list'


# In[ ]:

# fetch the list object of a query on Twitter API. Create it if it doesn't exist
def get_list(api, query_id):
    list_name = list_name_from_query(query_id)
    the_list = None
    try:
        all_lists = api.lists_all(screen_name=config['owner_screen_name'])
    except Exception as e: 
        print("Can't reach the list of list with Twitter API:", e)
        raise OurException
    else:
        for x in all_lists:
            if x.name == list_name:
                the_list = x
                # print("List found")
                break

    if the_list is None:
        try:
            the_list = api.create_list(name=list_name, mode="private", include_rts=False)
        except Exception as e: 
            print("Can't create the list '{}' with Twitter API: {}".format(list_name, e))
            raise OurException
        else:
            print("List created")
    
    return the_list


# In[ ]:

# add the users with tweets about a query to the list of posters for this query on Twitter API
# BUG: Twitter API bug ? Sometimes not all users are added?
def add_posters_to_list(api, query_id, the_list, bulk_size=90):
    new_posters = list(posters_collection.find({"search_queries": query_id, 
                                                "added_to_list": {"$ne": the_list.full_name},
                                                "original_poster": True}))

    for n in range(0, len(new_posters), bulk_size):  # you can only add < 100 users to a list at a time
        if bulk_size + the_list.member_count > 4998:  # Twitter allows a maximum of 5000 users in a list
            print("List full, ignore members addition")
            break
            
        np_slice_twitter_ids = [x["twitter_user"]["id"] for x in new_posters[n:n+bulk_size]]
        np_slice_ids = [x["_id"] for x in new_posters[n:n+bulk_size]]

        try:
            api.add_list_members(user_id=np_slice_twitter_ids, list_id=the_list.id)
        except Exception as e:
            print("Can't add people to the list with Twitter API:", e)
            # raise Exception
        else:
            for i in np_slice_ids:  # flag posters as added to the list in mongodb so we don't retry later
                posters_collection.update_one({ "_id": i }, {"$addToSet": {"added_to_list": the_list.full_name}})


# In[ ]:

# complete fetch process for one query on list
def run_list(api, search_query, query_id):
    # Get/create list
    the_list = get_list(api, query_id)

    # Get posters added by the other script since last execution:
    add_posters_to_list(api, query_id, the_list)

    get_new_tweets(api, "list", search_query, query_id, the_list)


# ## Run

# In[ ]:

# A simple wrapper to keep the process alive in case of unexpected exceptions
def run_safe(f, *args, **kwargs):
    try:
        f(*args, **kwargs)
    except OurException:
        print("Collect failed.")
        return


# In[ ]:

# get all queries from config and execute them sequentially
# called every x minutes (x defined in config.json)
def run_all():
    global config
    try:
        with open("config.json") as f:
            config = json.load(f)
    except Exception as e:
        print("WARNING: INCORRECT JSON CONFIG! USING THE ONE IN MEMORY. FIX FAST.\nException:", e)
        
    try:
        api = get_twitter_api()
    except Exception as e:
        print("Can't connect to twitter. Collect failed:", e)
    
    for query in config["queries"]:
        if "search" in query["sources"]:
            run_safe(get_new_tweets, api, "search", query["keywords"], query["id"])
        if "list" in query["sources"]:
            run_safe(run_list, api, query["keywords"], query["id"])


# In[ ]:

# run the whole process continuously
def run_scheduled():
    last_run = datetime.now() - timedelta(days=1)
    while True:
        if datetime.now() - timedelta(minutes=config["run_every_in_minutes"]) > last_run:
            last_run = datetime.now()
            run_all()
        else:
            time.sleep(1)


# In[ ]:

# the actual (unique) function call
run_scheduled()


# ### Run if you need to reinit the state of EVERY url (for debugging purposes)

# In[ ]:

def add_all_urls():
    for status in tqdm(status_collection.find()):
        for url_dict in status["tweet"]["entities"].get("urls", []):
            update_url(status["tweet"]["id"], url_dict["expanded_url"], status["clean_text"], status["sources"][0])


# In[ ]:

#add_all_urls()


# ## Notes:
# - Les recherches avec filtres URL, #, plusieurs mots-clefs, OR... donnent les mêmes résultats qu'en filtrant à la main.
# - Les listes n'apportent pas beaucoup de résultats en plus (mais potentiellement beaucoup de bruit)
# - La recherche trouve les variantes très proches: "nips2016" -> "#NIPS2016", "NIPS 2016", "(NIPS) 2016", "Neural Information Processing Systems 2016"...
# - Il n'est pas nécessaire de filtrer les résultats de la recherche nips2016 par le keyword nips2016, c'est bien fait par l'API
